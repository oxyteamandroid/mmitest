
package com.android.testphone;

import com.android.testphone.R;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class PromptDialog extends Dialog {
    private TextView mMessageView;
    private TextView mTitleView;
    private Button mButtonPositive;

    private View.OnClickListener mOnClickListener;

    public void setOnClickListener(View.OnClickListener listener) {
        mOnClickListener = listener;
    }

    public PromptDialog(Context context) {
        super(context);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        setContentView(R.layout.alert_dialog);

        findViews();
        setCancelable(false);
    }

    private void findViews() {
        mTitleView = (TextView) getWindow().findViewById(R.id.alertTitle);
        mMessageView = (TextView) getWindow().findViewById(R.id.message);
        mButtonPositive = (Button) getWindow().findViewById(R.id.button1);
        mButtonPositive.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dismiss();

                if (mOnClickListener != null) {
                    mOnClickListener.onClick(v);
                }
            }

        });
    }

    public void setTitle(int resId) {
        mTitleView.setText(resId);
    }

    public void setMessage(int resId) {
        mMessageView.setText(resId);
    }

    public void setButtonText(int resId) {
        mButtonPositive.setText(resId);
    }
}
