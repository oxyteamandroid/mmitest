
package com.android.testphone;

import java.util.HashMap;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.provider.Settings;
import android.util.Log;

public class InterceptKeyDialog extends Dialog {

    private ListView mListView;
    private PromptDialog mDialog;
    private Context mContext;
    private boolean mIsRound;

    public InterceptKeyDialog(Context context, int theme) {
        super(context, theme);
        mContext = context;
        mIsRound = Utils.isRoundScreen();
        View view;
        if (mIsRound) {
            view = View.inflate(context, R.layout.test_manu_round, null);
        } else {
            view = View.inflate(context, R.layout.test_manu_square, null);
        }
        // setTitle(R.string.mmi_test);
        getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        setCancelable(false);
        setContentView(view);
        setCanceledOnTouchOutside(false);

        mListView = (ListView) view.findViewById(android.R.id.list);

    }

    public void setListAdapter(BaseAdapter adapter) {
        mListView.setAdapter(adapter);
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener listener) {
        mListView.setOnItemClickListener(listener);
    }

    public void setOnKeyDialogDismissListener(DialogInterface.OnDismissListener listener) {
        setOnDismissListener(listener);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mDialog == null) {
            mDialog = new PromptDialog(mContext);
            mDialog.setTitle(R.string.prompt);
            mDialog.setMessage(R.string.has_not_test_all_case);
            mDialog.setButtonText(R.string.confirm);
            mDialog.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }

            });
        }

        boolean result = false;
        switch (keyCode) {
            case KeyEvent.KEYCODE_HOME:
            case KeyEvent.KEYCODE_BACK:
                mDialog.show();
                result = true;
                break;

            case KeyEvent.KEYCODE_POWER:
                dismiss();
                // Reset the value of mmi_homekey_dispatched to 0 for home key
                // work.
                break;

        }

        return result;
    }

}
