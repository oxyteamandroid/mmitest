
package com.android.testphone;

public class Item {
    String name;
    String translate;

    public String getName() {
        return name;
    }

    public String getTranslate() {
        return translate;
    }

    @Override
    public String toString() {
        return "name:" + name + ",translate:" + translate;
    }
}
