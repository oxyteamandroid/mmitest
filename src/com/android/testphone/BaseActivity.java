
package com.android.testphone;

import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

public class BaseActivity extends Activity {
    private static final String TAG = "BaseActivity";
    private String SUB_TAG = "";
    protected static final String KEY_STATUS = "item_test_status";
    protected static final int KEY_STATUS_INVALID_BACK = -1;
    protected static final int KEY_STATUS_SUCCESS = 0;
    protected static final int KEY_STATUS_FAIL = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        shadowHome();
        super.onCreate(savedInstanceState);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    private void shadowHome() {
        getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD);
    }

    protected void setActivityResult(int key_status, int resultCode) {
        Intent intent = new Intent();
        intent.putExtra(KEY_STATUS, key_status);
        setResult(resultCode, intent);
    }

    public boolean onKeyDown(int KeyCode, KeyEvent msg) {
        switch (KeyCode) {
            case KeyEvent.KEYCODE_HOME:
                Toast.makeText(this, getText(R.string.has_not_test_all_case), Toast.LENGTH_SHORT)
                        .show();
                return true;
        }
        return super.onKeyDown(KeyCode, msg);
    }

    protected void displayView(View view) {
        if (null != view && view.getVisibility() != View.VISIBLE) {
            view.setVisibility(View.VISIBLE);
        }
    }

    protected void dismissView(View view) {
        if (null != view && view.getVisibility() != View.GONE) {
            view.setVisibility(View.GONE);
        }
    }

    protected void setSubTag(String tag) {
        SUB_TAG = "[" + tag + "] ";
    }

    protected void logd(String msg) {
        Log.d(TAG, SUB_TAG + msg);
    }

    protected void logw(String msg) {
        Log.w(TAG, SUB_TAG + msg);
    }

    protected void loge(String msg) {
        Log.e(TAG, SUB_TAG + msg);
    }

    protected void logv(String msg) {
        Log.v(TAG, SUB_TAG + msg);
    }

    protected void logi(String msg) {
        Log.i(TAG, SUB_TAG + msg);
    }
}
