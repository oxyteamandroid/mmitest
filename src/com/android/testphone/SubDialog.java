
package com.android.testphone;

import com.android.testphone.views.BaseView;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

public class SubDialog extends Dialog implements View.OnClickListener {
    private int mResult = BaseView.KEY_STATUS_INVALID_BACK_FOR_DIALOG;

    private BaseView mView;

    public SubDialog(Context context, OnDismissListener listener) {
        super(context, R.style.Dialog_style);
        getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        setCanceledOnTouchOutside(false);
        setOnDismissListener(listener);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(view);
        mView = (BaseView) view;
    }

    @Override
    public void setContentView(View view,
            android.view.ViewGroup.LayoutParams params) {
        super.setContentView(view, params);
        mView = (BaseView) view;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mView.onStop();
    }

    public void onResume() {
        mView.onResume();
    }

    public int getResult() {
        return mResult;
    }

    public void setResult(int result) {
        mResult = result;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm_btn_pass:
                mResult = BaseView.KEY_STATUS_SUCCESS_FOR_DIALOG;
                break;
            case R.id.confirm_btn_fail:
                mResult = BaseView.KEY_STATUS_FAIL_FOR_DIALOG;
                break;
        }

        if (mView != null)
            mView.onFinish();
        dismiss();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mView != null) {
                mView.onFinish();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

}
