
package com.android.testphone.views;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbManager;
import android.view.View;
import android.widget.Button;

import com.android.testphone.R;
import com.android.testphone.Utils;

public class UsbTest extends BaseView {

    private static final String SUB_TAG = "UsbTest";

    private View mUsbInsert;
    private View mUsbTest;

    private static int getLayout() {
        if (Utils.isRoundScreen())
            return R.layout.usb_test_insert_for_dialog_round;
        else
            return R.layout.usb_test_insert_for_dialog_square;
    }

    public UsbTest(Context context, OnClickListener listener) {
        super(context, getLayout(), listener, SUB_TAG);

        findViewsForDialog();
    }

    private void findViewsForDialog() {
        mUsbInsert = findViewById(R.id.usb_test_inser);
        mUsbTest = findViewById(R.id.usb_test);
    }

    private void switchView(View view) {
        if (view == mUsbInsert) {
            mUsbInsert.setVisibility(View.VISIBLE);
            mUsbTest.setVisibility(View.GONE);
        } else if (view == mUsbTest) {
            mUsbTest.setVisibility(View.VISIBLE);
            mUsbInsert.setVisibility(View.GONE);
        }
    }

    public void onStop() {
        mContext.unregisterReceiver(mIntentReceiver);
    }

    public void onResume() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_UMS_CONNECTED);
        filter.addAction(Intent.ACTION_MEDIA_MOUNTED);
        filter.addAction(Intent.ACTION_UMS_DISCONNECTED);
        filter.addAction(Intent.ACTION_MEDIA_REMOVED);
        filter.addAction(UsbManager.ACTION_USB_STATE);
        mContext.registerReceiver(mIntentReceiver, filter);
    }

    private void findViews() {
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            System.out.println("UsbTest onReceive action : " + action);
            if (action.equals(Intent.ACTION_UMS_CONNECTED)
                    || action.equals(Intent.ACTION_MEDIA_MOUNTED)) {
                logv("connected or mounted action : " + action);
                switchView(mUsbTest);
                findViews();
            } else if (action.equals(Intent.ACTION_UMS_DISCONNECTED)
                    || action.equals(Intent.ACTION_MEDIA_REMOVED)) {
                logv("disconnected or removed action : " + action);
                switchView(mUsbInsert);
            } else if (action.equals(UsbManager.ACTION_USB_STATE)) {
                if (intent.getExtras().getBoolean("connected")) {
                    logv("connected or mounted action : " + action);
                    switchView(mUsbTest);
                    findViews();
                } else {
                    logv("disconnected or removed action : " + action);
                    switchView(mUsbInsert);
                }
            }
        }
    };
}
