
package com.android.testphone.views;

import android.content.Context;
import android.view.Surface;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.android.testphone.R;

public class GyrsensorTest extends BaseView {

    private static final String SUB_TAG = "GyrsensorTest";

    private TextView gText;
    private ImageView mImage;
    private TextView gValues;
    private Sensor mSensor;
    private SensorManager sManager;
    MyGsensorListener mGListener;

    public GyrsensorTest(Context context, OnClickListener listener) {
        super(context, R.layout.gyr_sensor, listener, SUB_TAG);

        mGListener = new MyGsensorListener();
        sManager = (SensorManager) mContext
                .getSystemService(Context.SENSOR_SERVICE);
        mSensor = sManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        sManager.registerListener(mGListener,
                mSensor, SensorManager.SENSOR_DELAY_UI);

        findViews();
        gText.setText(R.string.gyr_sensor);
        // CurrentDisplayInfo(R.string.g_up, R.drawable.gsensor_up);
        mImage.setImageResource(R.drawable.pointer_right);

    }

    private class MyGsensorListener implements SensorEventListener {
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        public void onSensorChanged(SensorEvent event) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_GYROSCOPE: {

                    onGsensorChanged(
                            event.values[0], event.values[1], event.values[2]);
                    break;
                }
            }
        }
    }

    private void onGsensorChanged(float x, float y, float z) {
        gValues.setText("X:" + x + "\n" + "Y:" + y + "\n" + "Z:" + z);
    }

    private void initView(int rotation) {
        switch (rotation) {
            case Surface.ROTATION_0:
                // CurrentDisplayInfo(R.string.g_up, R.drawable.gsensor_up);
                mImage.setImageResource(R.drawable.pointer_up);
                break;
            case Surface.ROTATION_90:
                // CurrentDisplayInfo(R.string.g_left, R.drawable.gsensor_left);
                mImage.setImageResource(R.drawable.pointer_left);
                break;
            case Surface.ROTATION_180:
                // CurrentDisplayInfo(R.string.g_down, R.drawable.gsensor_down);
                mImage.setImageResource(R.drawable.pointer_down);
                break;
            case Surface.ROTATION_270:
                // CurrentDisplayInfo(R.string.g_right,
                // R.drawable.gsensor_right);
                mImage.setImageResource(R.drawable.pointer_right);
                break;
        }
    }

    private void CurrentDisplayInfo(int str_id, int img_id) {
        gText.setText(str_id);
        mImage.setImageResource(img_id);
    }

    private void findViews() {
        gText = (TextView) findViewById(R.id.g_text);
        gValues = (TextView) findViewById(R.id.g_value);
        gValues.setText("X:0.0\nY:0.0\nZ:0.0");
        mImage = (ImageView) findViewById(R.id.g_image);
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    public void onFinish() {
        if (sManager != null) {
            sManager.unregisterListener(mGListener);
        }
    }
}
