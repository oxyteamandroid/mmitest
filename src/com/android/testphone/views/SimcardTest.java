
package com.android.testphone.views;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.widget.Button;
import android.widget.TextView;

import com.android.testphone.R;

public class SimcardTest extends BaseView {

    private static final String SUB_TAG = "SimcardTest";

    public SimcardTest(Context context, OnClickListener listener) {
        super(context, R.layout.sim_card_test, listener, SUB_TAG);
        findViews();
    }

    private boolean hasIccCard() {
        TelephonyManager telephonyManager = (TelephonyManager) mContext
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (telephonyManager.hasIccCard()) {
            return true;
        }
        return false;
    }

    private void findViews() {
        TextView simStatus = (TextView) findViewById(R.id.sim_status);
        simStatus.setText(hasIccCard() ? R.string.sim_ok : R.string.sim_no);
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }
}
