
package com.android.testphone.views;

import android.content.Context;
import android.media.MediaPlayer;
import android.widget.Button;

import com.android.testphone.R;
import com.android.testphone.Utils;

public class AudioTest extends BaseView {
    private static final String SUB_TAG = "AudioTest";

    private MediaPlayer mMp = null;

    private static int getLayout() {
        if (Utils.isRoundScreen())
            return R.layout.audio_test_round;
        else
            return R.layout.audio_test_square;
    }

    public AudioTest(Context context, OnClickListener listener) {
        super(context, getLayout(), listener, SUB_TAG);

        findViews();
        audioPlay();
    }

    private void findViews() {
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        passButton.setOnClickListener(mOnClickListener);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        failButton.setOnClickListener(mOnClickListener);
    }

    public void onFinish() {
        stopAndReleaseMediaPlayer();
    }

    private void audioPlay() {
        stopAndReleaseMediaPlayer();

        mMp = MediaPlayer.create(mContext, R.raw.factorytest);
        if (null == mMp) {
            loge("create mediaplay fail !");
            return;
        }
        mMp.setVolume(7.0f, 7.0f);
        mMp.setLooping(true);

        try {
            mMp.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            loge("start mediaplay exception !");
        }

        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(1500);
                } catch (Exception e) {
                    loge("Thread sleep Exception");
                }
            }
        }).start();
    }

    private void stopAndReleaseMediaPlayer() {
        if (mMp != null) {
            try {
                mMp.stop();
                mMp.release();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
