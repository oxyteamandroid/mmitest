
package com.android.testphone.views;

import android.content.Context;
import android.view.Surface;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.android.testphone.R;

public class OrisensorTest extends BaseView {

    private static final String SUB_TAG = "OrisensorTest";

    private TextView gText;
    private ImageView mImage;
    private TextView gValues;
    private Sensor mSensor;
    private Sensor aSensor;
    private SensorManager sManager;
    MyGsensorListener mGListener;
    float[] accelerometerValues = new float[3];
    float[] magneticFieldValues = new float[3];
    float[] values = new float[3];
    float[] rotate = new float[9];

    public OrisensorTest(Context context, OnClickListener listener) {
        super(context, R.layout.mag_sensor, listener, SUB_TAG);

        mGListener = new MyGsensorListener();
        sManager = (SensorManager) mContext
                .getSystemService(Context.SENSOR_SERVICE);
        mSensor = sManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        aSensor = sManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        sManager.registerListener(mGListener,
                mSensor, SensorManager.SENSOR_DELAY_UI);
        sManager.registerListener(mGListener,
                aSensor, SensorManager.SENSOR_DELAY_UI);

        findViews();
        gText.setText(R.string.o_sensor);
        // CurrentDisplayInfo(R.string.g_up, R.drawable.gsensor_up);
        mImage.setImageResource(R.drawable.pointer_right);

    }

    private class MyGsensorListener implements SensorEventListener {
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        public void onSensorChanged(SensorEvent event) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_ACCELEROMETER: {
                    accelerometerValues = event.values;
                    break;
                }
                case Sensor.TYPE_MAGNETIC_FIELD: {
                    magneticFieldValues = event.values;
                    break;
                }
            }
            SensorManager.getRotationMatrix(rotate, null, accelerometerValues, magneticFieldValues);
            SensorManager.getOrientation(rotate, values);
            values[0] = (float) Math.toDegrees(values[0]);
            onGsensorChanged(values[0], values[1], values[2]);
        }
    }

    private void onGsensorChanged(float x, float y, float z) {
        gValues.setText("Orientation:" + x);
    }

    private void initView(int rotation) {
        switch (rotation) {
            case Surface.ROTATION_0:
                // CurrentDisplayInfo(R.string.g_up, R.drawable.gsensor_up);
                mImage.setImageResource(R.drawable.pointer_up);
                break;
            case Surface.ROTATION_90:
                // CurrentDisplayInfo(R.string.g_left, R.drawable.gsensor_left);
                mImage.setImageResource(R.drawable.pointer_left);
                break;
            case Surface.ROTATION_180:
                // CurrentDisplayInfo(R.string.g_down, R.drawable.gsensor_down);
                mImage.setImageResource(R.drawable.pointer_down);
                break;
            case Surface.ROTATION_270:
                // CurrentDisplayInfo(R.string.g_right,
                // R.drawable.gsensor_right);
                mImage.setImageResource(R.drawable.pointer_right);
                break;
        }
    }

    private void CurrentDisplayInfo(int str_id, int img_id) {
        gText.setText(str_id);
        mImage.setImageResource(img_id);
    }

    private void findViews() {
        gText = (TextView) findViewById(R.id.g_text);
        gValues = (TextView) findViewById(R.id.g_value);
        gValues.setText("X:0.0\nY:0.0\nZ:0.0");
        mImage = (ImageView) findViewById(R.id.g_image);
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    public void onFinish() {
        if (sManager != null) {
            sManager.unregisterListener(mGListener);
        }
    }
}
