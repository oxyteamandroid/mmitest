
package com.android.testphone.views;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

public class BaseView extends LinearLayout {
    private static final String TAG = "BaseView";
    private static final String VIEWS_PACKAGE = "com.android.testphone.views.";

    private String SUB_TAG = "";

    public static final int KEY_STATUS_INVALID_BACK_FOR_DIALOG = -1;
    public static final int KEY_STATUS_SUCCESS_FOR_DIALOG = 0;
    public static final int KEY_STATUS_FAIL_FOR_DIALOG = 1;

    protected OnClickListener mOnClickListener;

    public BaseView(Context context, int layout) {
        super(context);
        initLayout(context, layout);
    }

    public BaseView(Context context, int layout, OnClickListener listener, String subTag) {
        this(context, layout);
        mOnClickListener = listener;
        setSubTag(subTag);
    }

    public void onStart() {

    }

    public void onStop() {

    }

    public void onResume() {

    }

    public BaseView(Context context, OnClickListener listener, String subTag) {
        super(context);
        mOnClickListener = listener;
        setSubTag(subTag);
    }

    public void onFinish() {

    }

    private void initLayout(Context context, int layout) {
        View.inflate(context, layout, this);
    }

    protected void setSubTag(String tag) {
        SUB_TAG = "[" + tag + "] ";
    }

    protected void logd(String msg) {
        Log.d(TAG, SUB_TAG + msg);
    }

    protected void logw(String msg) {
        Log.w(TAG, SUB_TAG + msg);
    }

    protected void loge(String msg) {
        Log.e(TAG, SUB_TAG + msg);
    }

    protected void logv(String msg) {
        Log.v(TAG, SUB_TAG + msg);
    }

    protected void logi(String msg) {
        Log.i(TAG, SUB_TAG + msg);
    }

    protected void displayView(View view) {
        if (null != view && view.getVisibility() != View.VISIBLE) {
            view.setVisibility(View.VISIBLE);
        }
    }

    protected void dismissView(View view) {
        if (null != view && view.getVisibility() != View.GONE) {
            view.setVisibility(View.GONE);
        }
    }

    @SuppressWarnings("unchecked")
    public static BaseView createContentView(String className, Context context,
            OnClickListener listener) {
        try {
            if (className.indexOf(".") == -1) {
                className = VIEWS_PACKAGE + className;
            }
            Class c = Class.forName(className);
            Constructor constructor = c.getDeclaredConstructor(Context.class,
                    OnClickListener.class);

            /*
             * if(className.equals("SoftwareVersion")){ return new
             * SoftwareVersion(context,listener); } else
             * if(className.equals("TouchScreenTest")){ return new
             * TouchScreenTest(context,listener); }
             */

            return (BaseView) constructor.newInstance(context, listener);
        } catch (ClassNotFoundException e) {
            Log.e(TAG, "Type not found: " + className, e);
        } catch (NoSuchMethodException e) {
            // Impossible to reach here.
            Log.e(TAG, "No such constructor.", e);
        } catch (InvocationTargetException e) {
            Log.e(TAG, "Unexpected InvocationTargetException", e);
        } catch (IllegalAccessException e) {
            Log.e(TAG, "Unexpected IllegalAccessException", e);
        } catch (InstantiationException e) {
            Log.e(TAG, "Unexpected InstantiationException", e);
        }
        return null;
    }

    public static boolean isInstallApk(Context context, String packagename) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(
                    packagename, 0);
        } catch (NameNotFoundException e) {
            packageInfo = null;
            // e.printStackTrace();
        }
        if (packageInfo == null) {
            return false;
        } else {
            return true;
        }
    }
}
