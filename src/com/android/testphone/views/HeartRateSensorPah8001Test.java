package com.android.testphone.views;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.testphone.R;

public class HeartRateSensorPah8001Test extends BaseView {

    private static final String SUB_TAG = "HeartRatePah8001Test";
    private String exe_path = "/data/data/com.android.testphone/test_pah8001";
    private String command1 = " --test=1 --time=8";
    private String command2 = " --test=2 --time=8";
    private String[] cmd1 = new String[] { "/system/bin/sh", "-c", exe_path + command1 };
    private String[] cmd2 = new String[] { "/system/bin/sh", "-c", exe_path + command2 };
    private TextView tv_test1;
    private TextView tv_test2;
    private Button test1;
    private Button test2;
    private Button passButton;
    private Button failButton;
    private File exe_file;
    private String sensorDataStr = "";

    public HeartRateSensorPah8001Test(Context context, OnClickListener listener) {
        super(context, R.layout.heartrate_sensor_pah8001, listener, SUB_TAG);
        Log.d(SUB_TAG, "ProximitySensorTest contruct");
        test1 = (Button) findViewById(R.id.button1);
        test2 = (Button) findViewById(R.id.button2);
        tv_test1 = (TextView) findViewById(R.id.tvtest1);
        tv_test2 = (TextView) findViewById(R.id.tvtest2);
        findViews();
        try {
            copyData(exe_path);
            exe_file = new File(exe_path);
            exe_file.setExecutable(true, true);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        test1.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v) {
                passButton.setEnabled(false);
                passButton.setClickable(false);
                failButton.setEnabled(false);
                failButton.setClickable(false);
                test1.setEnabled(false);
                test1.setClickable(false);
                test2.setEnabled(false);
                test2.setClickable(false);
                getSensorData(cmd1,1);
            }
        });

        test2.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v) {
                passButton.setEnabled(false);
                passButton.setClickable(false);
                failButton.setEnabled(false);
                failButton.setClickable(false);
                test1.setEnabled(false);
                test1.setClickable(false);
                test2.setEnabled(false);
                test2.setClickable(false);
                getSensorData(cmd2,2);
            }
        });
    }

    /*
     *设置通过,失败的button
     */
    private void findViews() {
        passButton = (Button) findViewById(R.id.confirm_btn_pass);
        failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    /*
     *设置hander，接收getSensorData()的信息
     */
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        Log.e("test", "===handler===handler===");
        super.handleMessage(msg);
        switch (msg.what) {
            case 1:
                tv_test1.setText(sensorDataStr);
                break;
            case 2:
                tv_test2.setText(sensorDataStr);
                break;
            case 3:
                passButton.setEnabled(true);
                passButton.setClickable(true);
                failButton.setEnabled(true);
                failButton.setClickable(true);
                test1.setEnabled(true);
                test1.setClickable(true);
                test2.setEnabled(true);
                test2.setClickable(true);
                break;
            }
        }
    };

    /*
     *运行test_pah8001可执行文件,并将输出通过mHandler.sendEmptyMessage()
     *发送到屏幕上
     */
    private void getSensorData(final String[] cmd, final int i) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Process process = Runtime.getRuntime().exec(cmd);
                    BufferedInputStream in = new BufferedInputStream(process.getErrorStream());
                    final BufferedReader br = new BufferedReader(new InputStreamReader(in));
                    InputStreamReader ir = new InputStreamReader(process.getInputStream());
                    final BufferedReader input = new BufferedReader(ir);
                    try {
                        while ((sensorDataStr = input.readLine()) != null) {
                            mHandler.sendEmptyMessage(i);
                            try {
                                Thread.sleep(50);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                process.waitFor();
                mHandler.sendEmptyMessage(3);
                } catch (java.io.IOException e) {
                System.err.println("IOException " + e.getMessage());
                } catch (InterruptedException e) {
                throw new RuntimeException(e);
                }
            }
        }).start();
    }

    /*
     *将Assets中的可执行文件test_pah8001复制到指定目录下
     */
    private void copyData(String strOutFileName) throws IOException
        {
        InputStream myInput;
        OutputStream myOutput = new FileOutputStream(strOutFileName);
        myInput = this.getContext().getAssets().open("test_pah8001");
        byte[] buffer = new byte[1024];
        int length = myInput.read(buffer);
        while(length > 0)
            {
            myOutput.write(buffer, 0, length);
            length = myInput.read(buffer);
            }
        myOutput.flush();
        myInput.close();
        myOutput.close();
    }
}
