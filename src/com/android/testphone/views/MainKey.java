
package com.android.testphone.views;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import com.android.testphone.R;

public class MainKey extends BaseView implements DialogInterface.OnKeyListener {

    private static final String SUB_TAG = "MainKey";

    private Button mKeyBtnVolPlus = null;
    private Button mKeyBtnVolReduce = null;
    private Button mKeyBtnHome = null;
    private Button mKeyBtnSearch = null;
    private Button mKeyBtnMenu = null;
    private Button mKeyBtnBack = null;
    int count = 0;

    public MainKey(Context context, OnClickListener listener) {
        super(context, R.layout.main_key, listener, SUB_TAG);
        findViews();
    }

    private void findViews() {
        mKeyBtnVolPlus = (Button) findViewById(R.id.keybtnvolumeplus);
        mKeyBtnVolReduce = (Button) findViewById(R.id.keybtnvolumereduce);
        mKeyBtnHome = (Button) findViewById(R.id.keybtnhome);
        mKeyBtnSearch = (Button) findViewById(R.id.keybtnsearch);
        mKeyBtnMenu = (Button) findViewById(R.id.keybtnmenu);
        mKeyBtnBack = (Button) findViewById(R.id.keybtnback);
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
        mKeyBtnBack.setOnClickListener(onKeyClockListener);

        // add by jhxu for m801
        mKeyBtnHome.setVisibility(View.GONE);
        mKeyBtnSearch.setVisibility(View.GONE);
        mKeyBtnMenu.setVisibility(View.GONE);
        mKeyBtnVolPlus.setVisibility(View.GONE);
        mKeyBtnVolReduce.setVisibility(View.GONE);
    }

    private void setBottonColorDown(View view) {
        logi("setBottonColorDown  onKeyDown KeyEvent KeyCode =");
        if (null != view) {
            logi("setBottonColorDown  onKeyDown KeyEvent KeyCode =");
            view.setBackgroundColor(Color.YELLOW);
        }
    }

    private void setButtonColorUp(View view) {
        logi("setBottonColorUp  onKeyDown KeyEvent KeyCode =");
        if (null != view) {
            logi("setBottonColorUp  onKeyDown KeyEvent KeyCode =");
            view.setBackgroundColor(Color.GREEN);
        }
    }

    private OnClickListener onKeyClockListener = new OnClickListener() {

        public void onClick(View v) {
            count++;
            if (count % 2 == 0) {
                setButtonColorUp(v);
            } else {
                setBottonColorDown(v);
            }

        }
    };

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
        logi("onKey  onKey ccccccccccccccccc  KeyEvent KeyCode =");
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_POWER:
                    return true;
                case KeyEvent.KEYCODE_SEARCH:
                    setBottonColorDown(mKeyBtnSearch);
                    return true;
                case KeyEvent.KEYCODE_HOME:
                    logi("onKeyDown KeyEvent KeyCode =" + keyCode);
                    setBottonColorDown(mKeyBtnHome);
                    return true;
                case KeyEvent.KEYCODE_VOLUME_UP:
                    setBottonColorDown(mKeyBtnVolPlus);
                    return true;
                case KeyEvent.KEYCODE_VOLUME_DOWN:
                    setBottonColorDown(mKeyBtnVolReduce);
                    return true;
                case KeyEvent.KEYCODE_MENU:
                    setBottonColorDown(mKeyBtnMenu);
                    return true;
                case KeyEvent.KEYCODE_BACK:
                    logi("onKeyDown KeyEvent KeyCode =" + keyCode);
                    setBottonColorDown(mKeyBtnBack);
                    return true;
                default:
                    break;
            }
        } else if (event.getAction() == KeyEvent.ACTION_UP) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_POWER:
                    return true;
                case KeyEvent.KEYCODE_SEARCH:
                    setButtonColorUp(mKeyBtnSearch);
                    return true;
                case KeyEvent.KEYCODE_HOME:
                    logi("KeyEvent KeyCode =" + keyCode);
                    setButtonColorUp(mKeyBtnHome);
                    return true;
                case KeyEvent.KEYCODE_VOLUME_UP:
                    setButtonColorUp(mKeyBtnVolPlus);
                    return true;
                case KeyEvent.KEYCODE_VOLUME_DOWN:
                    setButtonColorUp(mKeyBtnVolReduce);
                    return true;
                case KeyEvent.KEYCODE_MENU:
                    setButtonColorUp(mKeyBtnMenu);
                    return true;
                case KeyEvent.KEYCODE_BACK:
                    setButtonColorUp(mKeyBtnBack);
                    return true;
                default:
                    break;
            }
        }
        return false;
    }
}
