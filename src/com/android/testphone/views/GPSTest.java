
package com.android.testphone.views;

import java.util.Iterator;

import android.content.Context;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.testphone.R;

public class GPSTest extends BaseView {

    private static final String SUB_TAG = "GPSTest";

    private LocationManager mLocationManager;
    private TextView myLocationText = null;
    private TextView SatelliteText = null;
    private TextView gpsok = null;
    private float strValue[] = {
            0, 0, 0
    };
    private float strValueTemp = 0;
    private static final String GPS_PROVIDER = LocationManager.GPS_PROVIDER;

    public GPSTest(Context context, OnClickListener listener) {
        super(context, R.layout.gps_test, listener, SUB_TAG);

        findViews();

        if (!Settings.Secure.isLocationProviderEnabled(mContext.getContentResolver(), GPS_PROVIDER)) {
            Settings.Secure.setLocationProviderEnabled(mContext.getContentResolver(), GPS_PROVIDER,
                    true);
        }

        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        if (mLocationManager.isProviderEnabled(GPS_PROVIDER)) {
            gpsok.setText(R.string.gps_state_ok);
        } else {
            gpsok.setText(R.string.gps_state_disable);
        }
        Location location = mLocationManager.getLastKnownLocation(GPS_PROVIDER);
        logd(" location: " + location);

        try {
            mLocationManager.requestLocationUpdates(GPS_PROVIDER, 2000, 0, mLocationListener);
            mLocationManager.addGpsStatusListener(statusListener);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            Toast.makeText(mContext, R.string.check_gps_fail, Toast.LENGTH_LONG).show();
        }

        updateWithNewLocation(location);
        updateGpsStatus(0, null);

        new Thread() {
            public void run() {
                int i = 0;
                while (i < 3) {
                    if (strValueTemp != 0) {
                        strValue[i++] = strValueTemp;
                    }
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }

    private final GpsStatus.Listener statusListener = new GpsStatus.Listener() {
        public void onGpsStatusChanged(int event) {
            logd("now gps status changed event:" + event);
            GpsStatus status = mLocationManager.getGpsStatus(null);
            updateGpsStatus(event, status);
        }
    };

    public void onFinish() {
        if (Settings.Secure.isLocationProviderEnabled(mContext.getContentResolver(), GPS_PROVIDER)) {
            Settings.Secure.setLocationProviderEnabled(mContext.getContentResolver(), GPS_PROVIDER,
                    false);
        }

        if (mLocationManager != null) {
            mLocationManager.removeGpsStatusListener(statusListener);
            mLocationManager.removeUpdates(mLocationListener);
        }
    }

    private void updateGpsStatus(int event, GpsStatus status) {
        StringBuffer mStringBuffe = new StringBuffer();
        if (status == null) {
            SatelliteText.setText(mContext.getString(R.string.gps_num) + "0");
        } else if (event == GpsStatus.GPS_EVENT_SATELLITE_STATUS) {
            int maxSatellites = status.getMaxSatellites();
            logd("==maxSatellites=" + maxSatellites);
            Iterator<GpsSatellite> it = status.getSatellites().iterator();
            int count = 0;
            while (it.hasNext()) {
                GpsSatellite s = it.next();
                count++;
                logd("count:" + count + "--s.getAzimuth():" + s.getAzimuth()
                        + "--s.getElevation():"
                        + s.getElevation() + "--s.getPrn():" + s.getPrn() + "--s.getSnr():"
                        + s.getSnr());
                if (s.getPrn() > 0) {
                    strValueTemp = s.getSnr();
                }
                mStringBuffe.append(s.getPrn()).append(" --> ").append(s.getSnr()).append("\n");
            }
            SatelliteText.setText(mContext.getString(R.string.gps_num) + mStringBuffe);
        }
    }

    private final LocationListener mLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            logd("onLocationChanged: " + location);
            updateWithNewLocation(location);
        }

        public void onProviderDisabled(String provider) {
            logd("onProviderDisabled: " + provider);
            gpsok.setText(R.string.gps_state_off);
            updateWithNewLocation(null);
        }

        public void onProviderEnabled(String provider) {
            logd("onProviderEnabled: " + provider);
            gpsok.setText(R.string.gps_state_on);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            logd("onStatusChanged: " + status);
        }
    };

    private void updateWithNewLocation(Location location) {
        String latLongString;
        myLocationText.invalidate();
        if (location != null) {
            double lat = location.getLatitude();
            double lng = location.getLongitude();
            latLongString = lat + "," + lng;
        } else {
            latLongString = "";
        }
        // updata the textview
        myLocationText.invalidate();
        myLocationText.setText("GPS : ... \n" + latLongString);
        myLocationText.invalidate();
    }

    private void findViews() {
        myLocationText = (TextView) findViewById(R.id.gpsvalue);
        Button pass_button = (Button) findViewById(R.id.confirm_btn_pass);
        Button fail_button = (Button) findViewById(R.id.confirm_btn_fail);
        SatelliteText = (TextView) findViewById(R.id.tvsatellites);
        gpsok = (TextView) findViewById(R.id.gps_ok);
        pass_button.setOnClickListener(mOnClickListener);
        fail_button.setOnClickListener(mOnClickListener);
    }
}
