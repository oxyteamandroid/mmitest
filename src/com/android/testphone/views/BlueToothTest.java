
package com.android.testphone.views;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.testphone.R;

public class BlueToothTest extends BaseView {

    private static final String SUB_TAG = "BlueToothTest";

    private BluetoothAdapter mBluetoothAdapter = null;
    private final int EVENT_BT_ENABLE = 1;
    private final int EVENT_BT_FIND_OTHER = 2;
    private final int EVENT_BT_CLOSED = 3;
    private final int OPEN_BLUETOOTH_TIMEOUT = 4;
    private final int OPEN_BLUETOOTH_NO_DEVICE = 5;
    private TextView textBtCount = null;
    private TextView textDevice = null;
    private TextView btOk = null;
    private LinearLayout mConfirmBtns = null;
    private String btName = "";
    private boolean isRegisterReceiver = false;
    private int findBtCount = 0;

    public BlueToothTest(Context context, OnClickListener listener) {
        super(context, R.layout.blue_tooth_test, listener, SUB_TAG);

        findViews();

        // The default local adapter, or null if
        // Bluetooth is not supported on this hardware platform.
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (null == mBluetoothAdapter) {
            mHandler.sendEmptyMessage(OPEN_BLUETOOTH_NO_DEVICE);
            return;
        }

        startEnableThread();
    }

    private void findViews() {
        textDevice = (TextView) findViewById(R.id.textdevice);
        textBtCount = (TextView) findViewById(R.id.TextFound);
        btOk = (TextView) findViewById(R.id.btok);
        mConfirmBtns = (LinearLayout) findViewById(R.id.blue_tooth_btn_ll);
        Button passButton = (Button) mConfirmBtns
                .findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) mConfirmBtns
                .findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    private void startEnableThread() {
        new Thread() {
            public void run() {
                boolean enable = false;
                if (!mBluetoothAdapter.isEnabled())
                    mBluetoothAdapter.enable();
                logd("mBluetoothAdapter enable: " + enable);
                // enable() is an asynchronous call: it will return immediately,
                // maybe the following check is useless, but for secure.
                int count = 0;
                while (!mBluetoothAdapter.isEnabled()) {
                    count++;
                    try {
                        sleep(800);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (count >= 25) {
                        mHandler.sendEmptyMessage(OPEN_BLUETOOTH_TIMEOUT);
                        return;
                    }
                }
                mHandler.sendEmptyMessage(EVENT_BT_ENABLE);
            }
        }.start();
    }

    private void startSearchBT() {
        mBluetoothAdapter.startDiscovery();
        btOk.setText(R.string.bt_search);
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        mContext.registerReceiver(mReceiver, filter);
        isRegisterReceiver = true;
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // find devices
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent
                        .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    if (device.getName() != null) {
                        btName = btName + "\n" + device.getName() + " : "
                                + device.getAddress();
                        logd("find device:" + btName);
                    }
                    mHandler.sendEmptyMessage(EVENT_BT_FIND_OTHER);
                }
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED
                    .equals(action)) {
                // search finished
                logd("ACTION_DISCOVERY_FINISHED");
                mHandler.sendEmptyMessage(EVENT_BT_CLOSED);
            }
        }
    };

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String st = mContext.getString(R.string.bt_found);
            switch (msg.what) {
                case EVENT_BT_ENABLE:
                    logd("we start SearchBT");
                    startSearchBT();
                    break;
                case EVENT_BT_FIND_OTHER:
                    logd("we find one");
                    // add and show the BT name or address
                    findBtCount++;
                    st = st + "" + findBtCount;
                    textBtCount.setText(st);
                    textDevice.setText(btName);
                    break;
                case EVENT_BT_CLOSED:
                    st = st + "" + findBtCount;
                    textBtCount.setText(st);
                    btOk.setText(R.string.bt_ok);
                    textDevice.setText(btName);
                    mBluetoothAdapter.disable();
                    mConfirmBtns.setVisibility(View.VISIBLE);
                    break;
                case OPEN_BLUETOOTH_TIMEOUT:
                    btOk.setText(R.string.bt_open_timeout);
                    mConfirmBtns.setVisibility(View.VISIBLE);
                    break;
                case OPEN_BLUETOOTH_NO_DEVICE:
                    btOk.setText(R.string.bt_open_no_dev);
                    mConfirmBtns.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };

    public void onFinish() {
        if (null != mBluetoothAdapter)
            mBluetoothAdapter.disable();
        try {
            if (isRegisterReceiver) {
                mContext.unregisterReceiver(mReceiver);
            }
        } catch (Exception e) {
            loge("onDestroy unregisterReceiver Exception");
        }
    }
}
