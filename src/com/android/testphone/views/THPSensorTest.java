
package com.android.testphone.views;

import android.content.Context;
import android.view.Surface;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.android.testphone.R;

public class THPSensorTest extends BaseView {

    private static final String SUB_TAG = "THPSensorTest";

    private TextView gText;
    private ImageView mImage;
    private TextView tValues;
    private TextView hValues;
    private TextView pValues;
    private Sensor mTSensor;
    private Sensor mHSensor;
    private Sensor mPSensor;
    private SensorManager sManager;
    MyGsensorListener mGListener;

    public THPSensorTest(Context context, OnClickListener listener) {
        super(context, R.layout.thp_sensor, listener, SUB_TAG);

        mGListener = new MyGsensorListener();
        sManager = (SensorManager) mContext
                .getSystemService(Context.SENSOR_SERVICE);
        mTSensor = sManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        sManager.registerListener(mGListener,
                mTSensor, SensorManager.SENSOR_DELAY_UI);

        mHSensor = sManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        sManager.registerListener(mGListener,
                mHSensor, SensorManager.SENSOR_DELAY_UI);

        mPSensor = sManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        sManager.registerListener(mGListener,
                mPSensor, SensorManager.SENSOR_DELAY_UI);

        findViews();
        gText.setText(R.string.thp_test);
        // CurrentDisplayInfo(R.string.g_up, R.drawable.gsensor_up);
        mImage.setImageResource(R.drawable.pointer_right);

    }

    private class MyGsensorListener implements SensorEventListener {
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        public void onSensorChanged(SensorEvent event) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_AMBIENT_TEMPERATURE: {

                    onTGsensorChanged(
                            event.values[0], event.values[1], event.values[2]);
                    break;
                }
                case Sensor.TYPE_RELATIVE_HUMIDITY: {

                    onHGsensorChanged(
                            event.values[0], event.values[1], event.values[2]);
                    break;
                }
                case Sensor.TYPE_PRESSURE: {

                    onPGsensorChanged(
                            event.values[0], event.values[1], event.values[2]);
                    break;
                }

            }
        }
    }

    private void onTGsensorChanged(float x, float y, float z) {
        // tValues.setText("temp:"+ x);
    }

    private void onHGsensorChanged(float x, float y, float z) {
        // hValues.setText("humi:" + x);
    }

    private void onPGsensorChanged(float x, float y, float z) {
        pValues.setText("press:" + x);
    }

    private void findViews() {
        gText = (TextView) findViewById(R.id.thp_test);
        tValues = (TextView) findViewById(R.id.t_value);
        hValues = (TextView) findViewById(R.id.h_value);
        pValues = (TextView) findViewById(R.id.p_value);
        // tValues.setText("temp:0.0");
        // hValues.setText("humi:0.0");
        pValues.setText("press:0.0");
        mImage = (ImageView) findViewById(R.id.g_image);
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    public void onFinish() {
        if (sManager != null) {
            sManager.unregisterListener(mGListener);
        }
    }
}
