
package com.android.testphone.views;

import android.content.Context;
import android.view.Surface;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.android.testphone.R;

public class PressureSensorTest extends BaseView {

    private static final String SUB_TAG = "PressureSensorTest";

    private TextView gText;
    private ImageView mImage;
    private TextView pValues;
    private Sensor mPSensor;
    private SensorManager sManager;
    MyGsensorListener mPListener;

    public PressureSensorTest(Context context, OnClickListener listener) {
        super(context, R.layout.pressure_sensor, listener, SUB_TAG);

        mPListener = new MyGsensorListener();
        sManager = (SensorManager) mContext
                .getSystemService(Context.SENSOR_SERVICE);

        mPSensor = sManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        sManager.registerListener(mPListener,
                mPSensor, SensorManager.SENSOR_DELAY_UI);

        findViews();
        gText.setText(R.string.pressure_test);
        // CurrentDisplayInfo(R.string.g_up, R.drawable.gsensor_up);
        mImage.setImageResource(R.drawable.pointer_right);

    }

    private class MyGsensorListener implements SensorEventListener {
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        public void onSensorChanged(SensorEvent event) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_PRESSURE: {
                    onPGsensorChanged(
                            event.values[0], event.values[1], event.values[2]);
                    break;
                }

            }
        }
    }

    private void onPGsensorChanged(float x, float y, float z) {
        pValues.setText("pressure:" + x);
    }

    private void findViews() {
        gText = (TextView) findViewById(R.id.pressure_test);
        pValues = (TextView) findViewById(R.id.p_value);
        pValues.setText("pressure:0.0");
        mImage = (ImageView) findViewById(R.id.g_image);
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    public void onFinish() {
        if (sManager != null) {
            sManager.unregisterListener(mPListener);
        }
    }
}
