
package com.android.testphone.views;

import java.util.HashSet;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.testphone.R;
import com.android.testphone.SubDialog;
import com.android.testphone.Utils;

public class ChargeTest extends BaseView implements View.OnClickListener {

    private static final String SUB_TAG = "ChargeTest";

    private int oldChargeStatus = BatteryManager.BATTERY_STATUS_UNKNOWN;

    private TextView mStatus;
    private View mInInsert;
    private View mInTest;
    private View mOnTest;
    private View mInOut;

    private static int getLayout() {
        if (Utils.isRoundScreen())
            return R.layout.charge_for_dialog_round;
        else
            return R.layout.charge_for_dialog_square;
    }

    public ChargeTest(Context context, OnClickListener listener) {
        super(context, getLayout(), listener, SUB_TAG);

        findViewsForDialog();

        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        context.registerReceiver(mIntentReceiver, mIntentFilter);
    }

    private void findViewsForDialog() {
        mInInsert = findViewById(R.id.charge_in_insert);
        mInTest = findViewById(R.id.charge_in_test);
        mOnTest = findViewById(R.id.charge_on_test);
        mInOut = findViewById(R.id.charge_in_out);
    }

    private void switchView(View view) {
        if (view == mInInsert) {
            mInInsert.setVisibility(View.VISIBLE);
            mInTest.setVisibility(View.GONE);
            mOnTest.setVisibility(View.GONE);
            mInOut.setVisibility(View.GONE);
        } else if (view == mInTest) {
            mInTest.setVisibility(View.VISIBLE);
            mInInsert.setVisibility(View.GONE);
            mOnTest.setVisibility(View.GONE);
            mInOut.setVisibility(View.GONE);
        } else if (view == mOnTest) {
            mOnTest.setVisibility(View.VISIBLE);
            mInTest.setVisibility(View.GONE);
            mInInsert.setVisibility(View.GONE);
            mInOut.setVisibility(View.GONE);
        } else if (view == mInOut) {
            mInOut.setVisibility(View.VISIBLE);
            mInTest.setVisibility(View.GONE);
            mInInsert.setVisibility(View.GONE);
            mOnTest.setVisibility(View.GONE);
        }
    }

    public void onFinish() {
        mContext.unregisterReceiver(mIntentReceiver);
    }

    private void findViews() {
        mStatus = (TextView) findViewById(R.id.status);
        Button nextButton = (Button) findViewById(R.id.confirm_btn_next);
        Button failButton = (Button) findViewById(R.id.confirm_btn_mid_fail);
        nextButton.setOnClickListener(this);
        failButton.setOnClickListener(this);
    }

    private void findViews1() {
        mStatus = (TextView) findViewById(R.id.status);
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            logd("onReceive:" + action);
            if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {
                int plugType = intent.getIntExtra("plugged", 0);
                int status = intent.getIntExtra("status", BatteryManager.BATTERY_STATUS_UNKNOWN);
                String statusString;
                String chargeInString = null;
                System.out.println("status : " + status + ", plugType : " + plugType);
                if (status == BatteryManager.BATTERY_STATUS_CHARGING
                        || status == BatteryManager.BATTERY_STATUS_FULL) {
                    logd("BATTERY_STATUS_CHARGING");
                    switchView(mInTest);
                    findViews();
                    statusString = "\n" + mContext.getString(R.string.battery_info_status_charging);
                    if (plugType > 0) {
                        boolean isAc = (plugType == BatteryManager.BATTERY_PLUGGED_AC);
                        statusString = statusString + " " + mContext.getString(
                                (isAc) ? R.string.battery_info_status_charging_ac
                                        : R.string.battery_info_status_charging_usb);
                        chargeInString = mContext.getString(
                                (isAc) ? R.string.charger_in_ac
                                        : R.string.charger_in);
                    }
                    // mStatus.setText(statusString);
                    ((TextView) findViewById(R.id.charge_in_text)).setText(chargeInString);
                    oldChargeStatus = BatteryManager.BATTERY_STATUS_CHARGING;
                } else if (status == BatteryManager.BATTERY_STATUS_DISCHARGING) {
                    logd("BATTERY_STATUS_DISCHARGING");
                    statusString = mContext.getString(R.string.battery_info_status_discharging);
                } else if (status == BatteryManager.BATTERY_STATUS_NOT_CHARGING) {
                    logd("BATTERY_STATUS_NOT_CHARGING");
                    if (oldChargeStatus == BatteryManager.BATTERY_STATUS_CHARGING) {
                        switchView(mOnTest);
                        findViews1();
                        statusString = "\n"
                                + mContext.getString(R.string.battery_info_status_not_charging);
                        // mStatus.setText(statusString);
                    } else if (oldChargeStatus == BatteryManager.BATTERY_STATUS_UNKNOWN) {
                        switchView(mInInsert);
                    }
                } else if (status == BatteryManager.BATTERY_STATUS_FULL) {
                    logd("BATTERY_STATUS_FULL");
                    statusString = mContext.getString(R.string.battery_info_status_full);
                } else {
                    logd("BATTERY_STATUS_____UNKNOW");
                    statusString = mContext.getString(R.string.battery_info_status_unknown);
                }
                if (plugType == 0) {
                    if (oldChargeStatus == BatteryManager.BATTERY_STATUS_CHARGING) {
                        switchView(mOnTest);
                        findViews1();
                    } else if (oldChargeStatus == BatteryManager.BATTERY_STATUS_UNKNOWN) {
                        switchView(mInInsert);
                    }
                }
            }
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm_btn_next:
                switchView(mInOut);
                break;
            case R.id.confirm_btn_mid_fail:
                SubDialog subDialog = (SubDialog) mOnClickListener;
                onFinish();
                subDialog.setResult(BaseView.KEY_STATUS_FAIL_FOR_DIALOG);
                subDialog.dismiss();
                break;
        }

    }
}
