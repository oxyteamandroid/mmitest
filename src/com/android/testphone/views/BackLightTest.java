
package com.android.testphone.views;

import android.content.Context;
import android.os.Handler;
import android.os.IPowerManager;
import android.os.Message;
import android.os.PowerManager;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.android.testphone.R;
import com.android.testphone.Utils;

public class BackLightTest extends BaseView implements OnClickListener {

    private static final String SUB_TAG = "BackLightTest";

    private Button retestButton = null;
    private View mBackLightTesting;
    private View mBackLight;
    private boolean isOnPause = false;
    private final static long TIMER_DELAY = 500;
    private final static int MESSAGE_SET_RESULT_SCREEN = 1;
    private final static int MESSAGE_SHOW_BACKLIGHT = 2;
    private int mDefBrightness;

    private static int getLayout() {
        if (Utils.isRoundScreen())
            return R.layout.backlight_testing_for_dialog_round;
        else
            return R.layout.backlight_testing_for_dialog_square;
    }

    public BackLightTest(Context context, OnClickListener listener) {
        super(context, getLayout(), listener, SUB_TAG);

        mDefBrightness = getDefaultBrightness();
        sendShowBacklightDelayMessage(0);
        findViewsForDialog();
    }

    private void findViewsForDialog() {
        mBackLightTesting = findViewById(R.id.backlight_testing);
        mBackLight = findViewById(R.id.backlight);
    }

    private void switchView(int resId) {
        if (resId == R.id.backlight) {
            mBackLight.setVisibility(View.VISIBLE);
            mBackLightTesting.setVisibility(View.GONE);
        } else if (resId == R.id.backlight_testing) {
            mBackLightTesting.setVisibility(View.VISIBLE);
            mBackLight.setVisibility(View.GONE);
        }
    }

    private void sendResultScreenDelayMessage() {
        handler.sendEmptyMessageAtTime(MESSAGE_SET_RESULT_SCREEN, TIMER_DELAY);
    }

    private void sendShowBacklightDelayMessage(int count) {
        Message msg = handler.obtainMessage();
        msg.what = MESSAGE_SHOW_BACKLIGHT;
        msg.arg1 = count;
        handler.sendMessageDelayed(msg, TIMER_DELAY);
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_SET_RESULT_SCREEN:
                    switchView(R.id.backlight);
                    findViews();
                    break;
                case MESSAGE_SHOW_BACKLIGHT:
                    int count = msg.arg1;
                    logd("MESSAGE_SHOW_BACKLIGHT count = " + count);
                    if (isOnPause) {
                        return;
                    }
                    if (count == 5) {
                        setDefBrightness();
                        sendResultScreenDelayMessage();
                        return;
                    }
                    if ((count % 2) == 0) {
                        setDarkBrightness();
                    } else {
                        setLightBrightness();
                    }
                    sendShowBacklightDelayMessage(count + 1);
                    count++;
                    break;
            }
            super.handleMessage(msg);
        }
    };

    private void findViews() {
        retestButton = (Button) findViewById(R.id.Button01);
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
        retestButton.setOnClickListener(this);
    }

    private void setDefBrightness() {
        setBrightness(mDefBrightness);
    }

    private void setLightBrightness() {
        setBrightness(PowerManager.BRIGHTNESS_ON);
    }

    private void setDarkBrightness() {
        setBrightness(20);
    }

    private void setBrightness(int brightness) {
        try {
            IPowerManager power = IPowerManager.Stub.asInterface(
                    ServiceManager.getService(Context.POWER_SERVICE));
            if (null != power) {
                power.setTemporaryScreenBrightnessSettingOverride(brightness);
            } else {
                logw("setBrightness() power null");
            }
        } catch (RemoteException doe) {
            logw("setBrightness() RemoteException !!! ");
        }
    }

    private int getDefaultBrightness() {
        int brightness = getResources().getInteger(
                com.android.internal.R.integer.config_screenBrightnessDim);
        try {
            brightness = Settings.System.getInt(mContext.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS);
        } catch (SettingNotFoundException snfe) {
            loge("getDefaultBrightness() SettingNotFoundException !!!");
        }
        logv("getDefaultBrightness(): " + brightness);
        return brightness;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.Button01) {
            switchView(R.id.backlight_testing);
            sendShowBacklightDelayMessage(0);
        }

    }

    public void onFinish() {
        isOnPause = true;
    }
}
