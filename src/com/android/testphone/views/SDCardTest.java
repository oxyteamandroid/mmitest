
package com.android.testphone.views;

import android.content.Context;
import android.widget.Button;
import android.widget.TextView;

import com.android.testphone.R;

public class SDCardTest extends BaseView {
    private static final String SUB_TAG = "SDCardTest";

    public SDCardTest(Context context, OnClickListener listener) {
        super(context, R.layout.sd_card_test, listener, SUB_TAG);
        findViews();
    }

    private boolean checkFlash() {
        if (android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    private boolean checkSDCard() {
        return android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED);
    }

    private void findViews() {
        TextView sdStatus = (TextView) findViewById(R.id.memory_card_ok);
        TextView flashStatus = (TextView) findViewById(R.id.flash_ok);
        sdStatus.setText(checkSDCard() ? R.string.memory_card_ok : R.string.memory_card_no);
        flashStatus.setText(checkFlash() ? R.string.flash_ok : R.string.flash_no);
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }
}
