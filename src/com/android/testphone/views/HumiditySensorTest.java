
package com.android.testphone.views;

import android.content.Context;
import android.view.Surface;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.android.testphone.R;

public class HumiditySensorTest extends BaseView {

    private static final String SUB_TAG = "HumiditySensorTest";

    private TextView gText;
    private ImageView mImage;
    private TextView hValues;
    private Sensor mHSensor;
    private SensorManager sManager;
    MyGsensorListener mHListener;

    public HumiditySensorTest(Context context, OnClickListener listener) {
        super(context, R.layout.humidity_sensor, listener, SUB_TAG);

        mHListener = new MyGsensorListener();
        sManager = (SensorManager) mContext
                .getSystemService(Context.SENSOR_SERVICE);

        mHSensor = sManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        sManager.registerListener(mHListener,
                mHSensor, SensorManager.SENSOR_DELAY_UI);

        findViews();
        gText.setText(R.string.humidness_test);
        // CurrentDisplayInfo(R.string.g_up, R.drawable.gsensor_up);
        mImage.setImageResource(R.drawable.pointer_right);

    }

    private class MyGsensorListener implements SensorEventListener {
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        public void onSensorChanged(SensorEvent event) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_RELATIVE_HUMIDITY: {

                    onHGsensorChanged(
                            event.values[0], event.values[1], event.values[2]);
                    break;
                }
            }
        }
    }

    private void onHGsensorChanged(float x, float y, float z) {
        hValues.setText("humidity:" + x);
    }

    private void findViews() {
        gText = (TextView) findViewById(R.id.humidity_test);
        hValues = (TextView) findViewById(R.id.h_value);
        hValues.setText("humidity:0.0");
        mImage = (ImageView) findViewById(R.id.g_image);
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    public void onFinish() {
        if (sManager != null) {
            sManager.unregisterListener(mHListener);
        }
    }
}
