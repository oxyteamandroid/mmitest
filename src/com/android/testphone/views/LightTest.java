
package com.android.testphone.views;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import java.util.ArrayList;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.testphone.R;

public class LightTest extends BaseView implements SensorEventListener {

    private static final String SUB_TAG = "LightTest";

    SensorManager sensorManager = null;
    Sensor lightSensor = null;
    private final int HANDLE_ACCURACY = 1;
    private final int HANDLE_LIGHT_VALUE = 2;
    TextView accuracy_view = null;
    TextView value_0 = null;
    TextView light_test_values = null;
    TextView light_test_result = null;
    private ArrayList<Float> values = new ArrayList<Float>();

    static {
        System.loadLibrary("mmi_jni");
    }

    private native int lSensor_mmitest_enable();

    private native int lSensor_mmitest_disable();

    public LightTest(Context context, OnClickListener listener) {
        super(context, R.layout.light_display, listener, SUB_TAG);

        lSensor_mmitest_enable();
        getViews();

        sensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        if (null == sensorManager) {
            logw("Sensor.SENSOR_SERVICE unsupport !!! ");
            Toast.makeText(mContext, R.string.sensor_service_unsupport, Toast.LENGTH_LONG).show();
            return;
        }
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if (null == lightSensor) {
            logw("Sensor.TYPE_LIGHT not found !!! ");
            Toast.makeText(mContext, R.string.light_test_unsupport, Toast.LENGTH_LONG).show();
        }

        if (sensorManager != null && lightSensor != null) {
            sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_UI);
        }
    }

    private void getViews() {
        accuracy_view = (TextView) findViewById(R.id.accuracy);
        value_0 = (TextView) findViewById(R.id.value_0);
        light_test_values = (TextView) findViewById(R.id.light_test_values);
        light_test_result = (TextView) findViewById(R.id.light_test_result);
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);

    }

    private Handler mHandle = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case HANDLE_ACCURACY:
                    accuracy_view.setText(getResources().getString(R.string.light_test_accuracy)
                            + msg.arg1);
                    break;
                case HANDLE_LIGHT_VALUE:
                    Bundle b = (Bundle) msg.obj;
                    float value = b.getFloat("value_0");
                    value_0.setText(getResources().getString(R.string.light_test_value_0) + value);
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        if (sensor.getType() == Sensor.TYPE_LIGHT) {
            logv("accuracy: " + accuracy);
            if (mHandle != null) {
                Message msg = mHandle.obtainMessage();
                msg.what = HANDLE_ACCURACY;
                msg.arg1 = accuracy;
                mHandle.sendMessage(msg);
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (null == event.values || event.values.length == 0) {
            loge("ProximitySensor changed event.values invalid");
            return;
        }
        if (null == event.sensor || event.sensor.getType() != Sensor.TYPE_LIGHT) {
            loge("Light Sensor changed event.sensor or its type invalid");
            return;
        }
        float[] sensorValues = event.values;
        logv("value[0]:" + sensorValues[0]);
        logv("value[1]:" + sensorValues[1]);
        logv("value[2]:" + sensorValues[2]);
        if (values.size() < 4) {
            values.add(sensorValues[0]);
            addVlaue(values.size(), sensorValues[0], light_test_values);
        } else {
            showResult();
        }
        if (mHandle != null) {
            Message msg = mHandle.obtainMessage();
            msg.what = HANDLE_LIGHT_VALUE;
            Bundle arguments = new Bundle();
            arguments.putFloat("value_0", sensorValues[0]);
            msg.obj = arguments;
            mHandle.sendMessage(msg);
        }
        Message msg = mHandle.obtainMessage();
        msg.what = HANDLE_LIGHT_VALUE;
        Bundle arguments = new Bundle();
        arguments.putFloat("value_0", sensorValues[0]);
        msg.obj = arguments;
        mHandle.sendMessage(msg);
    }

    public void onFinish() {
        if (sensorManager != null && lightSensor != null) {
            sensorManager.unregisterListener(this, lightSensor);
        }

        lSensor_mmitest_disable();
    }

    private void showResult() {
        light_test_result.setText(R.string.light_test_result_success);
    }

    private void addVlaue(int index, float value, TextView textview) {
        if (null != textview) {
            textview.setText(textview.getText() + "\n"
                    + getResources().getString(R.string.light_test_order)
                    + index
                    + getResources().getString(R.string.light_test_group)
                    + value);
        }
    }
}
