
package com.android.testphone.views;

import com.android.testphone.R;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.widget.Button;

public class VibratorTest extends BaseView {
    private static final String SUB_TAG = "VibratorTest";

    private boolean mCancel = false;
    private final static int MSG_VIBRATOR_ENABLE = 1;
    private final static int MSG_VIBRATOR_AGAIN = 2;

    private Vibrator mVibrator;

    public VibratorTest(Context context, OnClickListener listener) {
        super(context, R.layout.vibrator_test, listener, SUB_TAG);
        findViews();
        mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        mHandler.sendEmptyMessageDelayed(MSG_VIBRATOR_ENABLE, 2000);
    }

    private void findViews() {
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    public void onFinish() {
        removeMessagesAndCancelVibrator();
    }

    private void removeMessagesAndCancelVibrator() {
        mCancel = true;
        if (mHandler.hasMessages(MSG_VIBRATOR_AGAIN)) {
            mHandler.removeMessages(MSG_VIBRATOR_AGAIN);
        }
        if (mHandler.hasMessages(MSG_VIBRATOR_ENABLE)) {
            mHandler.removeMessages(MSG_VIBRATOR_ENABLE);
        }
        if (null != mVibrator) {
            mVibrator.cancel();
        }
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_VIBRATOR_ENABLE:
                    mVibrator.vibrate(2000);
                    mHandler.sendEmptyMessageDelayed(MSG_VIBRATOR_AGAIN, 3000);
                    break;
                case MSG_VIBRATOR_AGAIN:
                    if (!mCancel) {
                        mVibrator.vibrate(2000);
                        mHandler.sendEmptyMessageDelayed(MSG_VIBRATOR_AGAIN, 3000);
                    }
                    break;
            }
            super.handleMessage(msg);
        }
    };
}
