
package com.android.testphone.views;

import java.io.IOException;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManagerImpl;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.testphone.R;

public class CameraTest extends BaseView implements SurfaceHolder.Callback,
        OnClickListener {

    private static final String SUB_TAG = "CameraTest";

    private LinearLayout mBtnsLl = null;
    private TextView mTxtOpening = null;
    private Button mSwitchButton = null;
    private TextView mTxtHint = null;
    private Camera mCamera;
    private SurfaceView mSurfaceView;
    private SurfaceHolder mSurfaceHolder;
    private final int MSG_SHOW_CAMERA = 1;
    private int mDispWidth, mDispHeight;
    private int mCameraCnt = 0;
    // save two index for back and front facing camera if exists.
    private int[] mCameraFaceIndex = new int[] {
            -1, -1
    };
    // 0, 1 to get mCameraFaceIndex value
    private int mCurFaceIndex;

    public CameraTest(Context context, OnClickListener listener) {
        super(context, R.layout.camera_test, listener, SUB_TAG);

        DisplayMetrics dm = context.getResources().getDisplayMetrics(); // new
                                                                        // DisplayMetrics();
        // WindowManagerImpl.getDefault().getDefaultDisplay().getMetrics(dm);
        mDispWidth = dm.widthPixels;
        mDispHeight = dm.heightPixels / 2;

        findViews();

        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(CameraTest.this);

        mCameraCnt = Camera.getNumberOfCameras();
        logd("onResume() mCameraCnt: " + mCameraCnt);
        if (mCameraCnt < 1) {
            loge("No physical cameras available on this device");
            dismissView(mTxtOpening);
            displayView(mBtnsLl);
            displayView(mTxtHint);
            dismissView(mSwitchButton);
        } else {
            displayView(mTxtOpening);
            dismissView(mBtnsLl);
            mHandler.sendEmptyMessageDelayed(MSG_SHOW_CAMERA, 1000);
        }
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_SHOW_CAMERA:
                    /**
                     * mask to check camera facing. 3 means have both back and
                     * front camera, so, display the switch button; otherwise,
                     * just on face camera, so, dismiss the switch button. Note:
                     * there must be one face valid.
                     */
                    int mask = 0;
                    int cnt = 0;
                    for (int i = 0; i < mCameraCnt; i++) {
                        CameraInfo ci = new CameraInfo();
                        Camera.getCameraInfo(i, ci);
                        logd("show camera index: " + i + " face: " + ci.facing);
                        if (ci.facing == CameraInfo.CAMERA_FACING_BACK
                                && -1 == mCameraFaceIndex[0]) {
                            mCameraFaceIndex[0] = i;
                            mask |= 0x01;
                            cnt++;
                        } else if (ci.facing == CameraInfo.CAMERA_FACING_FRONT
                                && -1 == mCameraFaceIndex[1]) {
                            mCameraFaceIndex[1] = i;
                            mask |= 0x02;
                            cnt++;
                        }
                        if (cnt >= 2)
                            break;
                    }
                    // open a valid camera
                    if (-1 != mCameraFaceIndex[0]) {
                        mCamera = Camera.open(mCameraFaceIndex[0]);
                        mCurFaceIndex = 0;
                    } else {
                        mCamera = Camera.open(mCameraFaceIndex[1]);
                        mCurFaceIndex = 1;
                    }
                    initCamera();
                    dismissView(mTxtOpening);
                    displayView(mBtnsLl);
                    dismissView(mTxtHint);
                    if ((mask & 0x03) == 3)
                        displayView(mSwitchButton);
                    else
                        dismissView(mSwitchButton);
                    break;
            }
        }
    };

    private void findViews() {
        mSurfaceView = (SurfaceView) findViewById(R.id.mSurfaceView);
        mTxtOpening = (TextView) findViewById(R.id.camera_opening);
        mBtnsLl = (LinearLayout) findViewById(R.id.camera_btns_ll);
        mTxtHint = (TextView) mBtnsLl.findViewById(R.id.camera_unsupport);
        mSwitchButton = (Button) mBtnsLl.findViewById(R.id.switch_button);
        Button passButton = (Button) mBtnsLl
                .findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) mBtnsLl
                .findViewById(R.id.confirm_btn_fail);
        mSwitchButton.setOnClickListener(this);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    private void initCamera() {
        if (null == mCamera) {
            loge("initCamera() fail mCamera null");
            return;
        }
        try {

            mCamera.setDisplayOrientation(90);
            Parameters parameters = mCamera.getParameters();
            Size size = parameters.getPreviewSize();
            logd("size: " + size.width + " X " + size.height);
            logd("disp:" + mDispWidth + " X " + mDispHeight);
            if (size.width > mDispWidth || size.height > mDispHeight) {
                parameters.setPreviewSize(mDispWidth, mDispHeight);
            }
            mCamera.setParameters(parameters);
            mCamera.setPreviewDisplay(mSurfaceHolder);
            mCamera.startPreview();
        } catch (IOException e) {
            loge("mSwitchButton 0 initCamera Exception!");
        }
    }

    public void onFinish() {
        if (mHandler != null && mHandler.hasMessages(MSG_SHOW_CAMERA)) {
            mHandler.removeMessages(MSG_SHOW_CAMERA);
        }
        mCameraFaceIndex[0] = -1;
        mCameraFaceIndex[1] = -1;
        mCurFaceIndex = -1;
        mCameraCnt = 0;
        releaseCamera();
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    public void surfaceChanged(SurfaceHolder surfaceholder, int format, int w,
            int h) {
        logi("Surface Changed");
    }

    public void surfaceCreated(SurfaceHolder surfaceholder) {
        logi("Surface Created");
    }

    public void surfaceDestroyed(SurfaceHolder surfaceholder) {
        logi("Surface Destroyed");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.switch_button) {
            logd("switch old: " + mCurFaceIndex + " new: " + (1 - mCurFaceIndex));
            // release old and open the other face.
            releaseCamera();
            mCurFaceIndex = 1 - mCurFaceIndex;
            mCamera = Camera.open(mCameraFaceIndex[mCurFaceIndex]);
            initCamera();
        }

    }
}
