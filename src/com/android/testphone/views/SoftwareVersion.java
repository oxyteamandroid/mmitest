
package com.android.testphone.views;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.android.testphone.R;
import com.android.testphone.Utils;

import android.content.Context;
import android.os.Build;
import android.os.SystemProperties;
import android.widget.Button;
import android.widget.TextView;

public class SoftwareVersion extends BaseView {
    private static final String SUB_TAG = "SoftwareVersion";

    private static final String FILENAME_PROC_VERSION = "/proc/version";

    private static int getLayout() {
        if (Utils.isRoundScreen())
            return R.layout.software_version_round;
        else
            return R.layout.software_version_square;
    }

    public SoftwareVersion(Context context, OnClickListener listener) {

        super(context, getLayout(), listener, SUB_TAG);
        setSubTag(SUB_TAG);
        findViews();
    }

    private void findViews() {
        TextView firmware = (TextView) findViewById(R.id.sv_firmware_value);
        firmware.setText(Build.VERSION.RELEASE);
        TextView dev_mode = (TextView) findViewById(R.id.sv_dev_mode_value);
        dev_mode.setText(Build.MODEL);
        TextView baseband = (TextView) findViewById(R.id.sv_baseband_ver_value);
        baseband.setText(SystemProperties.get("gsm.version.baseband",
                getResources().getString(R.string.device_info_default)));
        TextView kernel = (TextView) findViewById(R.id.sv_kernel_ver_value);
        kernel.setText(getFormattedKernelVersion());
        TextView build = (TextView) findViewById(R.id.sv_build_ver_value);
        build.setText(Build.DISPLAY);

        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    private String getFormattedKernelVersion() {
        String procVersionStr;
        try {
            procVersionStr = readLine(FILENAME_PROC_VERSION);

            final String PROC_VERSION_REGEX =
                    "\\w+\\s+" + /* ignore: Linux */
                            "\\w+\\s+" + /* ignore: version */
                            "([^\\s]+)\\s+" + /* group 1: 2.6.22-omap1 */
                            "\\(([^\\s@]+(?:@[^\\s.]+)?)[^)]*\\)\\s+" + /*
                                                                         * group
                                                                         * 2:
                                                                         * (xxxxxx
                                                                         * @
                                                                         * xxxxx
                                                                         * .
                                                                         * constant
                                                                         * )
                                                                         */
                            "\\((?:[^(]*\\([^)]*\\))?[^)]*\\)\\s+" + /*
                                                                      * ignore:
                                                                      * (gcc ..)
                                                                      */
                            "([^\\s]+)\\s+" + /* group 3: #26 */
                            "(?:PREEMPT\\s+)?" + /* ignore: PREEMPT (optional) */
                            "(.+)"; /* group 4: date */

            Pattern p = Pattern.compile(PROC_VERSION_REGEX);
            Matcher m = p.matcher(procVersionStr);

            if (!m.matches()) {
                loge("Regex did not match on /proc/version: " + procVersionStr);
                return "Unavailable";
            } else if (m.groupCount() < 4) {
                loge("Regex match on /proc/version only returned " + m.groupCount()
                        + " groups");
                return "Unavailable";
            } else {
                return (new StringBuilder(m.group(1)).append("\n").append(
                        m.group(2)).append(" ").append(m.group(3)).append("\n")
                        .append(m.group(4))).toString();
            }
        } catch (IOException e) {
            loge("IO Exception when getting kernel version for Device Info screen");
            return "Unavailable";
        }
    }

    private String readLine(String filename) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filename), 256);
        try {
            return reader.readLine();
        } finally {
            reader.close();
        }
    }

}
