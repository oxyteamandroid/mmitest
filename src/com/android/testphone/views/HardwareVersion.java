
package com.android.testphone.views;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Process;
import java.lang.ProcessBuilder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.android.testphone.R;
import com.android.internal.util.MemInfoReader;
import android.text.format.Formatter;

import android.content.Context;
import android.os.Build;
import android.os.SystemProperties;
import android.widget.Button;
import android.widget.TextView;

public class HardwareVersion extends BaseView {
    private static final String SUB_TAG = "HardwareVersion";
    private static final String MMI_VERSION = "MMI-2.0";
    private static final String BIN_CAT = "/system/bin/cat";
    private static final String FILENAME_PROC_CPUINFO = "/proc/cpuinfo";
    private static final String FILENAME_PROC_STAT = "/proc/stat";
    private static final String FILENAME_PROC_MAX_FREQ = "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq";
    private static final String FILENAME_PROC_MIN_FREQ = "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq";
    private static final String FILENAME_PROC_CUR_FREQ = "/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq";

    MemInfoReader mMemInfoReader = new MemInfoReader();
    Context mContext;

    private String getCpuFreq(String whichFreq) {
        String result = "1000MHZ";
        ProcessBuilder cmd;
        try {
            String[] args = {
                    BIN_CAT, whichFreq
            };
            cmd = new ProcessBuilder(args);
            Process process = cmd.start();
            InputStream in = process.getInputStream();
            byte[] re = new byte[24];
            while (in.read(re) != -1) {
                result = result + new String(re);
            }
            in.close();
        } catch (IOException ex) {
            result = "N/A";
        }
        return result;
    }

    private String getCpuName() {
        try {
            FileReader fr = new FileReader(FILENAME_PROC_CPUINFO);
            BufferedReader br = new BufferedReader(fr);
            String text = br.readLine();
            String[] array = text.split(":\\s+", 2);
            for (int i = 0; i < array.length; i++) {
                loge("cpuinfo:" + array[i]);
            }
            return array[1];
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getCpuModel() {
        try {
            FileReader fr = new FileReader(FILENAME_PROC_CPUINFO);
            BufferedReader br = new BufferedReader(fr);
            String text = br.readLine();
            text = br.readLine();
            text = br.readLine();
            String[] array = text.split(":\\s+", 2);
            for (int i = 0; i < array.length; i++) {
                loge("cpuinfo:" + array[i]);
            }
            return array[1];
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getTotalMem() {
        return Formatter.formatShortFileSize(mContext, mMemInfoReader.getTotalSize());
    }

    private String getFreeMem() {
        return Formatter.formatShortFileSize(mContext, mMemInfoReader.getFreeSize());
    }

    private String getCachedMem() {
        return Formatter.formatShortFileSize(mContext, mMemInfoReader.getCachedSize());
    }

    private String getUsedMem() {
        return Formatter.formatShortFileSize(
                mContext,
                mMemInfoReader.getTotalSize() - mMemInfoReader.getFreeSize()
                        - mMemInfoReader.getCachedSize());
    }

    private String getUsedPercent() {
        long usedMem = mMemInfoReader.getTotalSize()
                - (mMemInfoReader.getFreeSize() + mMemInfoReader.getCachedSize());
        long percent = (usedMem * 100) / mMemInfoReader.getTotalSize();
        return percent + "%";
    }

    public HardwareVersion(Context context, OnClickListener listener) {
        super(context, R.layout.hardware_version, listener, SUB_TAG);
        mContext = context;
        setSubTag(SUB_TAG);
        mMemInfoReader.readMemInfo();
        findViews();
    }

    private void findViews() {
        TextView cpu_name = (TextView) findViewById(R.id.cpu_name_value);
        cpu_name.setText(getCpuName());
        TextView cpu_frq = (TextView) findViewById(R.id.cpu_frq_value);
        cpu_frq.setText(getCpuFreq(FILENAME_PROC_CUR_FREQ));
        TextView mem_used = (TextView) findViewById(R.id.mem_used_value);
        mem_used.setText(getUsedMem());
        TextView mem_used_percent = (TextView) findViewById(R.id.mem_used_percent_value);
        mem_used_percent.setText(getUsedPercent());
        TextView mmi_version = (TextView) findViewById(R.id.mmi_version_value);
        mmi_version.setText(MMI_VERSION);

        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }
}
