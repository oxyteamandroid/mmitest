
package com.android.testphone.views;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.content.IntentFilter;
import android.os.BatteryManager;

import com.android.testphone.R;

public class FactoryDataReset extends BaseView {

    private static final String SUB_TAG = "FactoryDataReset";

    private final static int DIALOG_RESET = 1;
    private final static int RESET_BOUNDARY_VALUE = 40;

    private int mBatteryLevel = 0;
    private int mBatteryStatus = 0;

    private Button resetPhone;

    public FactoryDataReset(Context context, OnClickListener listener) {
        super(context, R.layout.resetphone_display, listener, SUB_TAG);
        context.registerReceiver(mIntentReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        resetPhone = (Button) findViewById(R.id.resetPhone_retest2);
        resetPhone.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (mBatteryLevel < RESET_BOUNDARY_VALUE
                        && mBatteryStatus != BatteryManager.BATTERY_STATUS_CHARGING) {
                    // showAlertDialog(R.string.master_clear_battery_low,
                    // false);
                    resetPhone.setEnabled(false);
                    Toast.makeText(mContext, R.string.master_clear_battery_low, Toast.LENGTH_SHORT)
                            .show();
                } else if (mBatteryLevel < RESET_BOUNDARY_VALUE
                        && mBatteryStatus == BatteryManager.BATTERY_STATUS_CHARGING) {
                    // showAlertDialog(R.string.master_clear_keep_charging,
                    // true);
                    mContext.sendBroadcast(new Intent("android.intent.action.MASTER_CLEAR"));
                } else if (mBatteryLevel >= RESET_BOUNDARY_VALUE) {
                    mContext.sendBroadcast(new Intent("android.intent.action.MASTER_CLEAR"));
                }
            }
        });
    }

    @Override
    public void onFinish() {
        super.onFinish();
        mContext.unregisterReceiver(mIntentReceiver);
        System.out.println("FactoryDataReset----------------------------onFinish!");
    }

    private void showAlertDialog(int stringId, final boolean reset) {
        new AlertDialog.Builder(getContext())
                .setMessage(stringId)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (reset) {
                            mContext.sendBroadcast(new Intent("android.intent.action.MASTER_CLEAR"));
                        } else {
                            resetPhone.setEnabled(false);
                        }
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_BATTERY_CHANGED)) {
                mBatteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                mBatteryStatus = intent.getIntExtra(BatteryManager.EXTRA_STATUS,
                        BatteryManager.BATTERY_STATUS_UNKNOWN);
                if (mBatteryLevel >= RESET_BOUNDARY_VALUE
                        || mBatteryStatus == BatteryManager.BATTERY_STATUS_CHARGING) {
                    resetPhone.setEnabled(true);
                }
            }
        }
    };

    // private void showDialog(int id) {
    // switch(id) {
    // case DIALOG_RESET:
    // AlertDialog dialog = new AlertDialog.Builder(mContext)
    // .setTitle("")
    // .setMessage(R.string.reset_phone_confirm)
    // .setPositiveButton(R.string.reset_phone_confirm_sure,
    // new DialogInterface.OnClickListener() {
    // public void onClick(DialogInterface dialog, int which) {
    // mContext.sendBroadcast(new Intent("android.intent.action.MASTER_CLEAR"));
    // }
    // })
    // .setNegativeButton(R.string.reset_phone_confirm_cancle,
    // new DialogInterface.OnClickListener() {
    // public void onClick(DialogInterface dialog, int which) {}
    // }).create();
    //
    // dialog.show();
    // }
    // }
}
