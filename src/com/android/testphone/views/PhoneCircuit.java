
package com.android.testphone.views;

import android.content.Context;
import android.media.MediaRecorder;
import android.widget.Button;

import com.android.testphone.R;

public class PhoneCircuit extends BaseView {

    private static final String SUB_TAG = "PhoneCircuit";
    Recorder mRecorder;
    VUMeter mVUMeter;

    public PhoneCircuit(Context context, OnClickListener listener) {
        super(context, R.layout.mic_test, listener, SUB_TAG);

        findViews();
        mRecorder = new Recorder();
        // mRecorder.setOnStateChangedListener(this);
        mVUMeter.setRecorder(mRecorder);
        mRecorder.startRecording(MediaRecorder.OutputFormat.AMR_NB, ".amr", context);

    }

    private void findViews() {
        mVUMeter = (VUMeter) findViewById(R.id.uvMeter);
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    public void onFinish() {
        mRecorder.stopRecording();
    }
}
