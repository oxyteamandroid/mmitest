
package com.android.testphone.views;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.hardware.SensorManager;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.testphone.R;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.smartsense.Sensor;
import com.ingenic.iwds.smartsense.SensorEvent;
import com.ingenic.iwds.smartsense.SensorEventListener;
import com.ingenic.iwds.smartsense.SensorServiceManager;

/**
 * 演示 Sensor 的使用 通过 getDefaultSensor 获取 Sensor 对象，通过 registerListener 注册
 * SensorEventListener。 通过 SensorEventListener.onSensorChanged 监控 Sensor 事件， 通过
 * SensorEventListener.onAccuracyChanged 监控精度变化。 注意：这个demo只能在手表端使用，手机端不能使用。
 * 
 * @param <MyGsensorListener>
 */
public class StepCountTest extends BaseView {

    private static final String SUB_TAG = "StepCountTest";
    private static final String IWDS_DEVICE_APK = "com.ingenic.iwds.device";

    private ServiceClient mClient;
    private SensorServiceManager mService;

    private Sensor mStepCountSensor;
    private Context mContext;
    private TextView mStepCountText;
    private TextView mStepCountPromptText;

    private SensorManager sManager;
    private TextView gText;
    private ImageView mImage;
    private TextView gValues;
    private float mStepCountData;

    StepCountTest(Context context, OnClickListener listener) {
        super(context, R.layout.stepcount_sensor, listener, SUB_TAG);
        Log.d(SUB_TAG, "StepCountTest contruct");
        mContext = context;
        if (isInstallApk(context, IWDS_DEVICE_APK) == true) {
            Log.d(SUB_TAG, "StepCountTest contruct installed " + IWDS_DEVICE_APK);
            mClient = new ServiceClient(mContext, ServiceManagerContext.SERVICE_SENSOR,
                    new StepCount());
            mClient.connect();
            findViews();
            gText.setText(R.string.stepcount_test);
            mImage.setImageResource(R.drawable.pointer_right);
        } else {
            Log.d(SUB_TAG, "StepCount contruct not installed " + IWDS_DEVICE_APK);
            showfindViews();
            gText.setText(R.string.stepcount_test);
            // //CurrentDisplayInfo(R.string.g_up, R.drawable.gsensor_up);
            mImage.setImageResource(R.drawable.pointer_right);

        }
    }

    public void onFinish() {
        if (mClient != null) {
            mClient.disconnect();
            ;
        }
    }

    private class StepCount implements ServiceClient.ConnectionCallbacks {

        @Override
        public void onConnected(ServiceClient serviceClient) {
            // TODO Auto-generated method stub

            Log.d(SUB_TAG, "Sensor service connected");

            mService = (SensorServiceManager) serviceClient
                    .getServiceManagerContext();

            /* 获取所有Sensor列表 */
            ArrayList<Sensor> sensorList = (ArrayList<Sensor>) mService.getSensorList();

            Log.d(SUB_TAG, "=========================================");
            Log.d(SUB_TAG, "Dump Sensor List");
            for (int i = 0; i < sensorList.size(); i++) {
                Log.d(SUB_TAG, "Sensor: " + sensorList.get(i).toString());
            }
            Log.d(SUB_TAG, "=========================================");

            registerSensors();
            // buildView();

        }

        @Override
        public void onDisconnected(ServiceClient arg0, boolean arg1) {
            // TODO Auto-generated method stub
            Log.d(SUB_TAG, "Sensor service diconnected");
            unregisterSensors();

        }

        @Override
        public void onConnectFailed(ServiceClient arg0, ConnectFailedReason arg1) {
            // TODO Auto-generated method stub
            Log.d(SUB_TAG, "Sensor service connect fail");

        }

    }

    private void findViews() {
        gText = (TextView) findViewById(R.id.g_text);
        mStepCountPromptText = (TextView) findViewById(R.id.g_prompt);
        mStepCountPromptText.setText(R.string.stepcount_prompt);
        mStepCountText = (TextView) findViewById(R.id.g_value);
        mStepCountText.setText("Step Count: " + mStepCountData);
        mImage = (ImageView) findViewById(R.id.g_image);
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    private void showfindViews() {
        gText = (TextView) findViewById(R.id.g_text);
        mStepCountText = (TextView) findViewById(R.id.g_value);
        // mHeartRateText.setText("Please wait for a while ...");
        mStepCountText.setText(R.string.stepcount_value_failed);
        mImage = (ImageView) findViewById(R.id.g_image);
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    private void registerSensors() {
        /* 通过 getDefaultSensor 获取各个 Sensor */
        mStepCountSensor = mService.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if (mStepCountSensor != null) {
            mService.registerListener(mListener, mStepCountSensor, 0);
        }

    }

    private void unregisterSensors() {
        if (mStepCountSensor != null) {
            mService.unregisterListener(mListener, mStepCountSensor);
        }
    }

    private SensorEventListener mListener = new SensorEventListener() {

        /*
         * 监控 Sensor 数据变化
         */
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensorType == Sensor.TYPE_STEP_COUNTER) {
                Log.d(SUB_TAG, "Update Step Count : " + event.values[0]);
                mStepCountData = event.values[0];
            }
            mStepCountText.setText("Step Count: " + mStepCountData);
        }

        /*
         * 监控 Sensor 数据精度变化
         */
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            Log.d(SUB_TAG, "onAccuracyChanged: " + sensor + ", accuracy: "
                    + accuracy);

        }
    };
}
