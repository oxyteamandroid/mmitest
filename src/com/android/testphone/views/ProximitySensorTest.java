
package com.android.testphone.views;

import android.content.Context;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.android.testphone.R;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.smartsense.Sensor;
import com.ingenic.iwds.smartsense.SensorEvent;
import com.ingenic.iwds.smartsense.SensorEventListener;
import com.ingenic.iwds.smartsense.SensorServiceManager;

public class ProximitySensorTest extends BaseView {

    private static final String SUB_TAG = "ProximitySensorTest";
    private static final String IWDS_DEVICE_APK = "com.ingenic.iwds.device";

    private ServiceClient mClient;
    private SensorServiceManager mService;
    private Sensor mProximitySensor;

    private Context mContext;
    private TextView mProximityText;

    private float mProximityData;
    private float mProximityApproach;

    public ProximitySensorTest(Context context, OnClickListener listener) {
        super(context, R.layout.proximity_sensor, listener, SUB_TAG);
        Log.d(SUB_TAG, "ProximitySensorTest contruct");
        mContext = context;

        if (isInstallApk(context, IWDS_DEVICE_APK) == true) {
            mClient = new ServiceClient(mContext, ServiceManagerContext.SERVICE_SENSOR,
                    new Proximity());
            mClient.connect();
            findViews();
        } else {
            Log.d(SUB_TAG, "ProximitySensorTest contruct not installed " + IWDS_DEVICE_APK);
            noIwdsFindViews();
        }
    }

    public void onFinish() {
        if (mClient != null) {
            mClient.disconnect();
        }
    }

    private void findViews() {
        mProximityText = (TextView) findViewById(R.id.value_PSensor);
        mProximityText.setText(getResources().getString(R.string.psensor_test_value_0) + "?");
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    private void noIwdsFindViews() {
        mProximityText = (TextView) findViewById(R.id.value_PSensor);
        mProximityText.setText(getResources().getString(R.string.psensor_test_no_iwds));
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    private class Proximity implements ServiceClient.ConnectionCallbacks {

        @Override
        public void onConnectFailed(ServiceClient arg0, ConnectFailedReason arg1) {
            Log.d(SUB_TAG, "Sensor service diconnected");
            unregisterSensors();

        }

        @Override
        public void onConnected(ServiceClient serviceClient) {
            Log.d(SUB_TAG, "Sensor service connected");

            mService = (SensorServiceManager) serviceClient
                    .getServiceManagerContext();

            registerSensors();
        }

        @Override
        public void onDisconnected(ServiceClient arg0, boolean arg1) {
            Log.d(SUB_TAG, "Sensor service connect fail");

        }
    }

    private void registerSensors() {
        mProximitySensor = mService.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        if (mProximitySensor != null) {
            mService.registerListener(mListener, mProximitySensor, 0);
        }

    }

    private void unregisterSensors() {
        if (mProximitySensor != null) {
            mService.unregisterListener(mListener, mProximitySensor);
        }
    }

    private SensorEventListener mListener = new SensorEventListener() {

        /*
         * 监控 Sensor 数据变化
         */
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensorType == Sensor.TYPE_PROXIMITY) {
                Log.d(SUB_TAG, "Update Proximity : " + event.values[0]);
                Log.d(SUB_TAG, "Update approach : " + event.values[1]);
                mProximityData = event.values[0];
                mProximityApproach = event.values[1];
            }
            mProximityText.setText(getResources().getString(R.string.psensor_test_value_0)
                    + mProximityData + (mProximityApproach == 1.0 ? "接近" : "远离"));
        }

        /*
         * 监控 Sensor 数据精度变化
         */
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            Log.d(SUB_TAG, "onAccuracyChanged: " + sensor + ", accuracy: "
                    + accuracy);

        }
    };

}
