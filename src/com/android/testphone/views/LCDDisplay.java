
package com.android.testphone.views;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Message;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.testphone.R;

public class LCDDisplay extends BaseView {
    private static final String SUB_TAG = "LCDDisplay";

    private final int EVENT_ANIM_FINISHED = 1;

    private ImageView mAnimImageView;
    private TextView mLcdFinished;
    private LinearLayout mLcdBtnLL;
    private AnimationDrawable mAnimation;

    public LCDDisplay(Context context, OnClickListener listener) {
        super(context, R.layout.lcd_display, listener, SUB_TAG);
        findViews();
        mAnimImageView.setBackgroundResource(R.anim.lcdanimation);
        mAnimation = (AnimationDrawable) mAnimImageView.getBackground();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        logd("onWindowFocusChanged(" + hasFocus + ")");
        if (hasFocus) {
            mAnimation.start();
            mHandler.sendEmptyMessageDelayed(EVENT_ANIM_FINISHED, 6000);
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case EVENT_ANIM_FINISHED:
                    mAnimation.stop();
                    dismissView(mAnimImageView);
                    displayView(mLcdFinished);
                    displayView(mLcdBtnLL);
                    break;
            }
        }
    };

    private void findViews() {
        mAnimImageView = (ImageView) findViewById(R.id.imageanim);
        mLcdFinished = (TextView) findViewById(R.id.lcdfinished);
        mLcdBtnLL = (LinearLayout) findViewById(R.id.lcd_btn_ll);
        Button passButton = (Button) mLcdBtnLL.findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) mLcdBtnLL.findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

}
