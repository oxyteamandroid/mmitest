
package com.android.testphone.views;

import java.util.ArrayList;
import java.util.HashMap;

import com.android.testphone.SubDialog;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;

public class TouchScreenTest extends BaseView {

    private static final String SUB_TAG = "TouchScreenTest";

    public TouchScreenTest(Context context, OnClickListener listener) {
        super(context, listener, SUB_TAG);
        // DisplayMetrics dm = new DisplayMetrics();
        DisplayMetrics dm = context.getResources().getDisplayMetrics(); // new
                                                                        // DisplayMetrics();
        // context.getWindowManager().getDefaultDisplay().getMetrics(dm);
        MultiTouchView mtv = new MultiTouchView(context);
        mtv.initView(dm);

        addView(mtv);
    }

    private class MultiTouchView extends View {
        private int mWidth, mHeight;
        private final static int WIDTH_NUM = 8;
        private int mStep;
        private int mWNum, mHNum, mTotal;
        private float x1;
        private float y1;
        ArrayList<Rect> rect = new ArrayList<Rect>();
        HashMap<Rect, Boolean> maps = new HashMap<Rect, Boolean>();
        private boolean mFirst;

        public MultiTouchView(Context context) {
            super(context);
        }

        public void initView(DisplayMetrics dm) {
            mFirst = true;
            mWidth = dm.widthPixels;
            mHeight = dm.heightPixels;
            int step = mWidth / WIDTH_NUM;
            mStep = step;
            mWNum = WIDTH_NUM + (mWidth % step == 0 ? 0 : 1);
            mHNum = mHeight / step + (mHeight % step == 0 ? 0 : 1);
            mTotal = mWNum * mHNum;
            logd("w: " + mWidth + " h: " + mHeight + "step: " + step + " wn: " + mWNum + " hn: "
                    + mHNum + " total: " + mTotal);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            x1 = event.getX();
            y1 = event.getY();
            for (Rect r : rect) {
                int xx1 = r.left;
                int yy1 = r.top;
                int xx2 = r.right;
                int yy2 = r.bottom;

                if (x1 > xx1 && x1 < xx2 && y1 > yy1 && y1 < yy2) {
                    maps.remove(r);
                    maps.put(r, true);
                }
            }
            invalidate();
            return true;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            if (mFirst) {
                Paint paint = new Paint();
                paint.setColor(Color.RED);
                int jm = 1;
                for (int m = 1; m < mHeight; m += mStep, jm++) { // y
                    int jn = 1;
                    for (int n = 1; n < mWidth; n += mStep, jn++) { // x
                        Rect r = new Rect(n, m, mStep * jn - 1, mStep * jm - 1);
                        rect.add(r);
                        maps.put(r, false);
                        canvas.drawRect(r, paint);
                    }
                }
                mFirst = false;
            } else {
                int greenCount = 0;
                Paint paint = new Paint();
                for (Rect r : rect) {
                    if (!maps.get(r)) {
                        paint.setColor(Color.RED);
                    } else {
                        greenCount++;
                        paint.setColor(Color.GREEN);
                    }
                    canvas.drawRect(r, paint);
                }
                if (greenCount == mTotal) {
                    SubDialog dialog = (SubDialog) mOnClickListener;
                    dialog.setResult(BaseView.KEY_STATUS_SUCCESS_FOR_DIALOG);
                    logi("All touched, MainActivity.this.finish()");
                    dialog.dismiss();
                }
            }
        }
    }
}
