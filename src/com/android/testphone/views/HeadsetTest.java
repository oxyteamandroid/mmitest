
package com.android.testphone.views;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.view.View;
import android.widget.Button;

import com.android.testphone.PromptDialog;
import com.android.testphone.R;
import com.android.testphone.SubDialog;

public class HeadsetTest extends BaseView implements View.OnClickListener {

    private static final String SUB_TAG = "HeadsetTest";

    private IntentFilter mFmHeadsetIntentFilter;
    private AudioManager mAm;
    private MediaPlayer mMp;
    private final static int DIALOG_HEADSET_INSERT = 1;

    private enum CurrentState {
        IDEL, HEADSETPLUG, LEFT, RIGHT, MICRO;
    }

    private CurrentState mState;
    private boolean mHasMicro;

    private View mHeadSetRequire;
    private View mHeadSetRight;
    private View mHeadSetLeft;
    private View mHeadSetMicro;
    private View mHeadSetOk;

    private PromptDialog mDialog;

    public HeadsetTest(Context context, OnClickListener listener) {
        super(context, R.layout.headset_insert_require_for_dialog, listener, SUB_TAG);

        mFmHeadsetIntentFilter = new IntentFilter(Intent.ACTION_HEADSET_PLUG);
        mAm = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);

        findViewsForDialog();
    }

    private void findViewsForDialog() {
        mHeadSetRequire = findViewById(R.id.headset_insert_require);
        mHeadSetRight = findViewById(R.id.headset_test_right);
        mHeadSetLeft = findViewById(R.id.headset_test_left);
        mHeadSetMicro = findViewById(R.id.headset_test_micro);
        mHeadSetOk = findViewById(R.id.headset_insert_ok);
    }

    @Override
    public void onResume() {
        if (!mAm.isWiredHeadsetOn()) {
            mState = CurrentState.IDEL;
            switchView(mHeadSetRequire);
            showDialog(DIALOG_HEADSET_INSERT);
            mHasMicro = false;
        } else {
            mState = CurrentState.HEADSETPLUG;
        }

        mContext.registerReceiver(mFmHeadSetIntentReceiver, mFmHeadsetIntentFilter);
        setFtMicToRevOn(true);
    }

    private void setFtMicToRevOn(boolean on) {
    }

    public void onStop() {
        mContext.unregisterReceiver(mFmHeadSetIntentReceiver);

        stopAndReleaseAudioManager();
        setFtMicToRevOn(false);
    }

    private void stopAndReleaseAudioManager() {
        if (null != mMp) {
            mMp.stop();
            mMp.release();
            mMp = null;
        }
    }

    private final BroadcastReceiver mFmHeadSetIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Intent.ACTION_HEADSET_PLUG)) {
                stopAndReleaseAudioManager();
                // state - 0 for unplugged, 1 for plugged.
                int state = intent.getIntExtra("state", -1);
                if (state == 0) {
                    mState = CurrentState.IDEL;
                    switchView(mHeadSetRequire);
                    showDialog(DIALOG_HEADSET_INSERT);
                    setFtMicToRevOn(false);
                    mHasMicro = false;
                    logi("....hell.the headset is .....unplugged......");
                } else if (state == 1) {
                    logi("....hell.the headset is .....plugged......");
                    if (mState == CurrentState.IDEL) {
                        mDialog.dismiss();
                    }
                    switchView(mHeadSetOk);
                    findViewsInsert();
                    mState = CurrentState.HEADSETPLUG;
                    // microphone - 1 if headset has a microphone, 0 otherwise
                    int micro = intent.getIntExtra("microphone", -1);
                    mHasMicro = micro == 1 ? true : false;
                } else {
                    loge("....hell.the headset is .....error......");
                }

            }
        }
    };

    private void findViewsInsert() {
        Button in_pass = (Button) findViewById(R.id.btn_headset_in_pass);
        Button in_fail = (Button) findViewById(R.id.btn_headset_in_fail);
        in_pass.setOnClickListener(this);
        in_fail.setOnClickListener(this);
    }

    private void showDialog(int id) {
        switch (id) {
            case DIALOG_HEADSET_INSERT:
                if (mDialog == null) {
                    mDialog = new PromptDialog(mContext);

                    mDialog.setTitle(R.string.headset_no);
                    mDialog.setMessage(R.string.headset_test_dialog_msg);
                    mDialog.setButtonText(R.string.exit_headset_test);
                    mDialog.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            SubDialog subDialog = (SubDialog) mOnClickListener;
                            onFinish();
                            subDialog.dismiss();
                        }
                    });
                }

                mDialog.show();
        }
    }

    private void switchView(View view) {
        if (view == mHeadSetRequire) {
            mHeadSetRequire.setVisibility(View.VISIBLE);
            mHeadSetRight.setVisibility(View.GONE);
            mHeadSetLeft.setVisibility(View.GONE);
            mHeadSetMicro.setVisibility(View.GONE);
            mHeadSetOk.setVisibility(View.GONE);
        } else if (view == mHeadSetRight) {
            mHeadSetRight.setVisibility(View.VISIBLE);
            mHeadSetRequire.setVisibility(View.GONE);
            mHeadSetLeft.setVisibility(View.GONE);
            mHeadSetMicro.setVisibility(View.GONE);
            mHeadSetOk.setVisibility(View.GONE);
        } else if (view == mHeadSetLeft) {
            mHeadSetLeft.setVisibility(View.VISIBLE);
            mHeadSetRight.setVisibility(View.GONE);
            mHeadSetRequire.setVisibility(View.GONE);
            mHeadSetMicro.setVisibility(View.GONE);
            mHeadSetOk.setVisibility(View.GONE);
        } else if (view == mHeadSetMicro) {
            mHeadSetMicro.setVisibility(View.VISIBLE);
            mHeadSetRight.setVisibility(View.GONE);
            mHeadSetLeft.setVisibility(View.GONE);
            mHeadSetRequire.setVisibility(View.GONE);
            mHeadSetOk.setVisibility(View.GONE);
        } else if (view == mHeadSetOk) {
            mHeadSetOk.setVisibility(View.VISIBLE);
            mHeadSetRight.setVisibility(View.GONE);
            mHeadSetLeft.setVisibility(View.GONE);
            mHeadSetMicro.setVisibility(View.GONE);
            mHeadSetRequire.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        int newstatus = BaseView.KEY_STATUS_INVALID_BACK_FOR_DIALOG;

        switch (v.getId()) {
            case R.id.btn_headset_right_fail:
            case R.id.btn_headset_left_fail:
            case R.id.btn_headset_in_fail:
            case R.id.btn_headset_micro_fail:
                newstatus = BaseView.KEY_STATUS_FAIL_FOR_DIALOG;
                break;
            case R.id.btn_headset_left_pass:
                switchView(mHeadSetRight); // right loop
                findViewsRight();
                mState = CurrentState.RIGHT;
                audioPlay(R.raw.right);
                return;
            case R.id.btn_headset_right_pass:
                if (mHasMicro) {
                    // microphone
                    stopAndReleaseAudioManager();
                    mState = CurrentState.MICRO;
                    setFtMicToRevOn(true);
                    switchView(mHeadSetMicro);
                    findViewsMicro();
                    return;
                }
                newstatus = BaseView.KEY_STATUS_SUCCESS_FOR_DIALOG;
                break;
            case R.id.btn_headset_micro_pass:
                newstatus = BaseView.KEY_STATUS_SUCCESS_FOR_DIALOG;
                break;
            case R.id.btn_headset_in_pass:
                switchView(mHeadSetLeft); // Left loop
                findViewsLeft();
                mState = CurrentState.LEFT;
                audioPlay(R.raw.left);
                return;
        }

        mState = CurrentState.IDEL;
        SubDialog dialog = (SubDialog) mOnClickListener;
        dialog.setResult(newstatus);
        dialog.dismiss();
    }

    private void findViewsLeft() {
        Button left_pass = (Button) findViewById(R.id.btn_headset_left_pass);
        Button left_fail = (Button) findViewById(R.id.btn_headset_left_fail);
        left_pass.setOnClickListener(this);
        left_fail.setOnClickListener(this);

    }

    private void findViewsMicro() {
        Button micro_pass = (Button) findViewById(R.id.btn_headset_micro_pass);
        Button micro_fail = (Button) findViewById(R.id.btn_headset_micro_fail);
        micro_pass.setOnClickListener(this);
        micro_fail.setOnClickListener(this);
    }

    private void findViewsRight() {
        Button right_pass = (Button) findViewById(R.id.btn_headset_right_pass);
        Button right_fail = (Button) findViewById(R.id.btn_headset_right_fail);
        right_pass.setOnClickListener(this);
        right_fail.setOnClickListener(this);
    }

    private void audioPlay(int musicResource) {
        stopAndReleaseAudioManager();

        mMp = MediaPlayer.create(mContext, musicResource);
        mMp.setVolume(7.0f, 7.0f);
        mMp.setLooping(true);
        try {
            mMp.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }
}
