
package com.android.testphone.views;

import java.util.List;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.view.View;

import com.android.testphone.R;

public class WiFiTest extends BaseView implements View.OnClickListener {

    private static final String SUB_TAG = "WiFiTest";

    private final int START_SHOW_FINDED_WIFI = 1;
    private final int WIFI_TEST_TIME_OUT = 2;
    private final int WIFI_TEST_RETRY = 3;

    private List<ScanResult> mWifiList;
    private TextView wifiSearch = null;
    private LinearLayout retry_LinearLayout = null;
    private TextView wifiOk = null;
    private Button passButton = null;
    private Button failButton = null;
    private WifiManager mWifiManager;

    public WiFiTest(Context context, OnClickListener listener) {
        super(context, R.layout.wifi_test, listener, SUB_TAG);

        mWifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        mWifiManager.getConnectionInfo();
        init();
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case START_SHOW_FINDED_WIFI:
                    logd("we start Searched WIFI");
                    LookUpScan();
                    break;
                case WIFI_TEST_TIME_OUT:
                    displayView(passButton);
                    displayView(failButton);
                    displayView(retry_LinearLayout);
                    wifiOk.setText(R.string.wifi_no);
                    break;
                case WIFI_TEST_RETRY:
                    wifiSearch.setText(R.string.d1);
                    wifiOk.setText(R.string.wifi_search);
                    dismissView(retry_LinearLayout);
                    break;
            }
        }
    };

    private void init() {
        findViews();
        new Thread() {
            public void run() {
                if (mWifiManager != null) {
                    if (!mWifiManager.isWifiEnabled()) {
                        mWifiManager.setWifiEnabled(true);
                    }
                    logd("setWifiEnabled:true?????");
                    while (!mWifiManager.isWifiEnabled()) {
                        try {
                            sleep(800);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        logd("!mWifiManager.isWifiEnabled()");
                    }
                    if (mWifiManager.isWifiEnabled()) {
                        logd("setWifiEnabled:true!!!!");
                    }
                    StartScan();
                }
            }
        }.start();
    }

    public void StartScan() {
        mWifiManager.startScan();
        mWifiList = mWifiManager.getScanResults(); // get Scan Results
        logd("mWifiList:" + mWifiList);

        new Thread() {
            public void run() {
                int j = 0;
                while (mWifiList == null) {
                    logd("mWifiList == null");
                    mWifiList = mWifiManager.getScanResults();
                    // Log.d(LOG_TAG,"mWifiList ="+mWifiList+"mWifiList.size() ="+mWifiList.size());
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    j++;
                    if (j >= 20) {
                        mHandler.sendEmptyMessage(WIFI_TEST_TIME_OUT);
                        break;
                    }
                }
                if (mWifiList != null) {
                    logd("mWifiList != null :start LookUpScan");
                    // LookUpScan();
                    mHandler.sendEmptyMessage(START_SHOW_FINDED_WIFI);
                }
            }
        }.start();

    }

    public void LookUpScan() {// look search results
        StringBuilder stringBuilder = new StringBuilder();
        logd("mWifiList.size() =" + mWifiList.size());
        dismissView(retry_LinearLayout);
        dismissView(passButton);
        dismissView(failButton);
        for (int i = 0, size = mWifiList.size(); i < size; i++) {
            stringBuilder.append("Index_" + new Integer(i + 1).toString() + ":");
            stringBuilder.append((mWifiList.get(i)).toString());
            stringBuilder.setLength(30);
            stringBuilder.append("\n");
        }
        logd("stringBuilder:" + stringBuilder);
        wifiSearch.setText(stringBuilder);
        if (stringBuilder.length() < 1) {
            wifiOk.setText(R.string.wifi_no);
            displayView(retry_LinearLayout);
        } else {
            wifiOk.setText(R.string.wifi);
            // retry_LinearLayout.setVisibility(View.VISIBLE);
        }
        displayView(passButton);
        displayView(failButton);
    }

    private void findViews() {
        // wifiTitle = (TextView) findViewById(R.id.Textwifi);
        wifiSearch = (TextView) findViewById(R.id.d1);
        wifiOk = (TextView) findViewById(R.id.wifi_ok);
        passButton = (Button) findViewById(R.id.confirm_btn_pass);
        failButton = (Button) findViewById(R.id.confirm_btn_fail);
        Button retryButton = (Button) findViewById(R.id.wify_retry_button);
        retry_LinearLayout = (LinearLayout) findViewById(R.id.wify_retry_linearLayout);
        dismissView(passButton);
        dismissView(failButton);
        dismissView(retry_LinearLayout);

        retryButton.setOnClickListener(this);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.wify_retry_button) {
            mHandler.sendEmptyMessage(WIFI_TEST_RETRY);
        }

    }
}
