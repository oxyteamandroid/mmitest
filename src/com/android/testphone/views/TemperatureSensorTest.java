
package com.android.testphone.views;

import android.content.Context;
import android.view.Surface;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.android.testphone.R;

public class TemperatureSensorTest extends BaseView {

    private static final String SUB_TAG = "TemperatureSensorTest";

    private TextView gText;
    private ImageView mImage;
    private TextView tValues;
    private TextView hValues;
    private TextView pValues;
    private Sensor mTSensor;
    private Sensor mHSensor;
    private Sensor mPSensor;
    private SensorManager sManager;
    MyGsensorListener mTListener;

    public TemperatureSensorTest(Context context, OnClickListener listener) {
        super(context, R.layout.temperature_sensor, listener, SUB_TAG);
        mTListener = new MyGsensorListener();

        sManager = (SensorManager) mContext
                .getSystemService(Context.SENSOR_SERVICE);
        mTSensor = sManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        sManager.registerListener(mTListener,
                mTSensor, SensorManager.SENSOR_DELAY_UI);

        findViews();
        gText.setText(R.string.temperature_test);
        // CurrentDisplayInfo(R.string.g_up, R.drawable.gsensor_up);
        mImage.setImageResource(R.drawable.pointer_right);

    }

    private class MyGsensorListener implements SensorEventListener {
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        public void onSensorChanged(SensorEvent event) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_AMBIENT_TEMPERATURE: {

                    onTGsensorChanged(
                            event.values[0], event.values[1], event.values[2]);
                    break;
                }
            }
        }
    }

    private void onTGsensorChanged(float x, float y, float z) {
        tValues.setText("temp:" + x);
    }

    private void findViews() {
        gText = (TextView) findViewById(R.id.temperature_test);
        tValues = (TextView) findViewById(R.id.t_value);
        tValues.setText("temp:0.0");
        mImage = (ImageView) findViewById(R.id.g_image);
        Button passButton = (Button) findViewById(R.id.confirm_btn_pass);
        Button failButton = (Button) findViewById(R.id.confirm_btn_fail);
        passButton.setOnClickListener(mOnClickListener);
        failButton.setOnClickListener(mOnClickListener);
    }

    public void onFinish() {
        if (sManager != null) {
            sManager.unregisterListener(mTListener);
        }
    }
}
