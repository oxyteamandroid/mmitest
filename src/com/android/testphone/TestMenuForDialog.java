
package com.android.testphone;

import com.android.testphone.views.BaseView;
import com.android.testphone.views.MainKey;

import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.AdapterView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.util.EncodingUtils;

import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.content.DialogInterface.OnDismissListener;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.smartsense.Sensor;
import com.ingenic.iwds.smartsense.SensorEvent;
import com.ingenic.iwds.smartsense.SensorEventListener;
import com.ingenic.iwds.smartsense.SensorServiceManager;

public class TestMenuForDialog extends BaseActivity implements
        AdapterView.OnItemClickListener {
    private String SUB_TAG = "TestMenuForDialog";
    private TestManuListAdapter mAdapter;
    private InterceptKeyDialog mDialog;
    private List<Integer> mImgItemList = new ArrayList<Integer>();
    private List<Integer> pmImgItemList = new ArrayList<Integer>();
    private int mSelectPos;
    private int aSelectPos;
    private int itemPosition = -1;
    private int mMode;
    private final static int START_NEXT_CLASS_TEST = 1;
    private static final String TEST_PROC_LIST = "/proc/hardware/list";
    private static final String TEST_ITEM_ATTR = "mmi.item";

    // private final static int INDEX_MAIN_KEY = 2;
    private static final SubDialog NULL = null;
    private ListView mListView;
    private List<Item> testproclist;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        KeyguardManager mKeyGuardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        KeyguardLock lock = mKeyGuardManager.newKeyguardLock("TestMenuForDialog");
        lock.disableKeyguard();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Utils.isRoundScreen()) {
            setContentView(R.layout.manu_main_round);
        } else {
            setContentView(R.layout.manu_main_square);
        }
        setSubTag(SUB_TAG);

        testproclist = getProcTestList();

        mImgItemList = getImgSource(testproclist.size());
        mAdapter = new TestManuListAdapter(this, testproclist, mImgItemList);

        mMode = getIntent().getIntExtra("mode", -1);
        logd("G type TestMenuForDialog= " + this.getWindow().getAttributes().type);
        logd("G type TestMenuForDialog  mMode= " + mMode);
        initDialog();
        if (mMode == 1) {
            startActivityAuto(0);
            // startSubDialog(0);
        }

    }

    @Override
    protected void onDestroy() {
        // Reset the value of mmi_homekey_dispatched to 0 for home key work.
        super.onDestroy();
    }

    private void initDialog() {

        mDialog = new InterceptKeyDialog(TestMenuForDialog.this, R.style.Dialog_style);
        mDialog.setListAdapter(mAdapter);
        mDialog.setOnItemClickListener(this);
        mDialog.setOnKeyDialogDismissListener(mKeyDialogDismissListener);
        mDialog.show();
    }

    private List<Integer> getImgSource(int size) {
        ArrayList<Integer> testList = new ArrayList<Integer>();

        for (int i = 0; i < size; i++) {
            testList.add(R.drawable.btn_check_off);
        }
        return testList;
    }

    private int findPosition(int position) {
        int findPostion = 0;
        for (findPostion = 0; findPostion < mTestClassNames.length; findPostion++) {
            if (((Item) mAdapter.getItem(position)).name.equals(mTestClassNames[findPostion][0]))
                break;
        }

        return findPostion;
    }

    private List<String> getTestList() {
        ArrayList<String> testList = new ArrayList<String>();
        final String[] subitems = getResources().getStringArray(
                R.array.sub_activity_names);

        for (int i = 0, len = subitems.length; i < len; i++) {
            testList.add(subitems[i]);
        }
        return testList;
    }

    private List<Item> getProcTestList() {
        ArrayList<Item> testtmpProcList = new ArrayList<Item>();

        String res = null;
        Log.v(SUB_TAG, "start read proc to get test list");

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(TEST_PROC_LIST), 256);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            String itemAttr = TEST_ITEM_ATTR;
            while ((res = reader.readLine()) != null) {
                if (res.indexOf(itemAttr) >= 0) {
                    String resource = res.substring(itemAttr.length() + 1);
                    if (resource.length() > 0) {
                        logd("test item ========= " + resource);
                        String[] resArray = resource.split(",");
                        for (int i = 0; i < resArray.length; i++) {
                            String mValue = resArray[i];
                            logd("add test item ========= " + mValue);
                            Item item = new Item();
                            item = (Item) addItemList(mValue);
                            testtmpProcList.add(item);
                        }
                    } else
                        logd("test item is null not add\n");
                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return testtmpProcList;
    }

    private Item addItemList(String resource) {
        Item item = new Item();
        item.name = resource;
        if (resource.equals("software_version")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.software_version);
        } else if (resource.equals("touchkey")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.keypad_test);
        } else if (resource.equals("touchscreen")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.touch_screen_test);
        } else if (resource.equals("audioplay")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.audio_test);
        } else if (resource.equals("charge")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.charge_test);
        } else if (resource.equals("vibrator")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.vibrator_test);
        } else if (resource.equals("usb")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.usb_test);
        } else if (resource.equals("lcddisplay")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.lcd_test);
        } else if (resource.equals("gsensor")) { // 重力传感器
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.g_sensor);
        } else if (resource.equals("accelerometersensor")) { // 加速度传感器
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.accelerometer_sensor);
        } else if (resource.equals("gyrsensor")) { // 陀螺仪传感器
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.gyr_sensor);
        } else if (resource.equals("magsensor")) { // 磁传感器
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.mag_sensor);
        } else if (resource.equals("orisensor")) { // 方向传感器
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.o_sensor);
        } else if (resource.equals("bluetooth")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.blue_tooth_test);
        } else if (resource.equals("wifi")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.wifi_test);
        } else if (resource.equals("backlight")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.backlight_test);
        } else if (resource.equals("mic")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.mic_and_receiver_test);
        } else if (resource.equals("temperaturesensor")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.temperature_test);
        } else if (resource.equals("humiditysensor")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.humidness_test);
        } else if (resource.equals("pressuresensor")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.pressure_test);
        } else if (resource.equals("heartrate")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.heartrate_test);
        } else if (resource.equals("stepcount")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.stepcount_test);
        } else if (resource.equals("factory_data_reset")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.reset_phone_test);
        } else if (resource.equals("proximity")) {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.proximity_sensor_test);
        } else {
            item.translate = this.getApplicationContext().getResources()
                    .getString(R.string.test_item_not_found);
        }

        return item;
    }

    private FileInputStream openFileInput(File file) {
        // TODO Auto-generated method stub
        return null;
    }

    private void startActivityAuto(int position) {
        int selectPos = 0;
        itemPosition = position;
        aSelectPos = position;
        selectPos = findPosition(position);

        startSubDialog(selectPos);
    }

    private void startSubDialog(int position) {

        SubDialog subDialog = new SubDialog(TestMenuForDialog.this, mOnDismissListener);
        // BaseView view =
        // BaseView.createContentView(mAdapter.getItem(position).toString(),
        // TestMenuForDialog.this, subDialog);
        BaseView view = BaseView.createContentView(mTestClassNames[position][1],
                TestMenuForDialog.this, subDialog);
        // add by jhxu for large screen
        DisplayMetrics dm = getResources().getDisplayMetrics(); // new
                                                                // DisplayMetrics();
        // Display d = WindowManagerImpl.getDefault().getDefaultDisplay();
        // d.getMetrics(dm);
        subDialog.setContentView(view, new LayoutParams(dm.widthPixels, dm.heightPixels));

        // if(position == INDEX_MAIN_KEY){
        if (mTestClassNames[position].equals("MainKey")) {
            subDialog.setCancelable(false);
            MainKey mainKeyView = (MainKey) view;
            subDialog.setOnKeyListener(mainKeyView);
        }

        subDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        subDialog.show();
        subDialog.onResume();

    }

    public static boolean isInstallApk(Context context, String packagename) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(
                    packagename, 0);
        } catch (NameNotFoundException e) {
            packageInfo = null;
            // e.printStackTrace();
        }
        if (packageInfo == null) {
            return false;
        } else {
            return true;
        }
    }

    public void onItemClick(AdapterView<?> parent, View view, int position,
            long id) {
        itemPosition = position;
        aSelectPos = position;
        int currentPosition = -1;
        logd("TestMenuForDialog     onItemClick  position " + position + " id = " + id);
        currentPosition = findPosition(position);
        // HashMap<String,String> map=(HashMap<String,String>)((ListView)
        // parent).getItemAtPosition(position);
        // String content=map.get("TextView2");

        logd("TestMenuForDialog     onItemClick  position "
                + ((Item) mAdapter.getItem(position)).translate);
        Item item = (Item) parent.getItemAtPosition(position);
        Item item2 = (Item) mAdapter.getItem(position);

        startSubDialog(currentPosition);
    }

    private DialogInterface.OnDismissListener mKeyDialogDismissListener = new DialogInterface.OnDismissListener() {

        @Override
        public void onDismiss(DialogInterface dialog) {
            finish();
        }

    };
    private DialogInterface.OnDismissListener mOnDismissListener = new DialogInterface.OnDismissListener() {

        @Override
        public void onDismiss(DialogInterface dialog) {
            SubDialog subDialog = (SubDialog) dialog;
            int result = subDialog.getResult();
            int selectPos = mSelectPos;

            if (mMode == 1) { // auto MMI test
                selectPos = aSelectPos;
            } else {
                if (itemPosition != -1) {
                    selectPos = itemPosition;
                }
            }

            switch (result) {
                case BaseView.KEY_STATUS_SUCCESS_FOR_DIALOG:
                    mImgItemList.set(selectPos, R.drawable.btn_check_on);
                    mAdapter.notifyDataSetChanged();
                    break;
                case BaseView.KEY_STATUS_FAIL_FOR_DIALOG:

                    mImgItemList.set(selectPos, R.drawable.btn_close_normal);
                    mAdapter.notifyDataSetChanged();
                    break;
                case BaseView.KEY_STATUS_INVALID_BACK_FOR_DIALOG:
                    break;
            }
            itemPosition = -1;
            aSelectPos = selectPos;
            // boolean startDialog = true;

            if (result != BaseView.KEY_STATUS_INVALID_BACK_FOR_DIALOG) {
                if (mMode == 1 && mSelectPos < mImgItemList.size() - 1) {
                    mHandler.sendEmptyMessage(START_NEXT_CLASS_TEST);
                    // startDialog = false;
                }
            }

            // logd("start InterceptKeyDialog again!");
            // if(startDialog) mDialog.show();
        }

    };

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case START_NEXT_CLASS_TEST:
                    aSelectPos++;
                    logd("TestMenuForDialog Handler selectPos++ =" + aSelectPos);
                    // startSubDialog(mSelectPos);
                    startActivityAuto(aSelectPos);
                    break;
            }
        }
    };

    public boolean hasTestAllCase() {
        if (mImgItemList != null) {
            for (int i = 0, size = mImgItemList.size() - 1; i < size; i++) {
                if (mImgItemList.get(i) == R.drawable.btn_check_off) {
                    return false;
                }
            }
        }
        return true;
    }

    // private String[] mTestClassNames = { "SoftwareVersion",
    // /*"HardwareVersion",*/
    // "LCDDisplay", "MainKey", "TouchScreenTest",
    // "VibratorTest", "AudioTest", /*"SimcardTest",
    // "SDCardTest", */"ChargeTest", "UsbTest", /*"WiFiTest",*/
    // "GsensorTest", "GyrsensorTest", "MagsensorTest", "OrisensorTest",
    // "BlueToothTest", /*"HeadsetTest", */"BackLightTest", "PhoneCircuit",
    // /*"CameraTest",*/ "HeartRateSensorTest", "THPSensorTest",
    // "GPSTest",
    // /*
    // * Please put FactoryDataReset at the last item, it will clear
    // * memory data, and restart the phone
    // */
    // "FactoryDataReset"};
    // private String[][] mTestClassNames = {
    // {"软件版本","SoftwareVersion"},
    // /*"HardwareVersion",*/
    // {"LCD 检查", "LCDDisplay"},
    // {"按键测试", "MainKey"},
    // { "触摸屏测试","TouchScreenTest"},
    // { "马达测试", "VibratorTest"},
    // { "音频测试", "AudioTest"},
    // /*{ "SimcardTest","SimcardTest"},*/
    // /*{ "SDCardTest","SDCardTest"},*/
    // { "充电测试", "ChargeTest"},
    // { "USB 测试", "UsbTest"},
    // /*{ "WiFiTest", "WiFiTest"},*/
    // { "重力传感", "GsensorTest"},
    // { "陀螺仪传感", "GyrsensorTest"},
    // { "地磁传感", "MagsensorTest"},
    // { "方向传感", "OrisensorTest"},
    // { "蓝牙测试", "BlueToothTest"},
    // /*{ "HeadsetTest", "HeadsetTest"},*/
    // { "背光测试", "BackLightTest"},
    // { "麦克测试", "PhoneCircuit"},
    // /*{ "CameraTest", "CameraTest"},*/
    // { "心率传感", "HeartRateSensorTest"},
    // { "温湿气压", "THPSensorTest"},
    // { "GPS测试", "GPSTest"},
    //
    // /*
    // * Please put FactoryDataReset at the last item, it will clear
    // * memory data, and restart the phone
    // */
    // { "恢复出厂设置", "FactoryDataReset"},
    // };

    private String[][] mTestClassNames = {

            /* "HardwareVersion", */

            {
                    "software_version", "SoftwareVersion"
            },
            {
                    "touchkey", "MainKey"
            },
            {
                    "touchscreen", "TouchScreenTest"
            },
            {
                    "lcddisplay", "LCDDisplay"
            },
            {
                    "vibrator", "VibratorTest"
            },
            {
                    "audioplay", "AudioTest"
            },
            /* { "simcard","SimcardTest"}, */
            /* { "sdcard","SDCardTest"}, */
            {
                    "charge", "ChargeTest"
            },
            {
                    "usb", "UsbTest"
            },
            {
                    "wifi", "WiFiTest"
            },
            {
                    "gsensor", "GsensorTest"
            },
            {
                    "accelerometersensor", "AccelerometerTest"
            },
            {
                    "gyrsensor", "GyrsensorTest"
            },
            {
                    "magsensor", "MagsensorTest"
            },
            {
                    "orisensor", "OrisensorTest"
            },
            {
                    "bluetooth", "BlueToothTest"
            },
            /* { "Headset", "HeadsetTest"}, */
            {
                    "backlight", "BackLightTest"
            },
            {
                    "mic", "PhoneCircuit"
            },
            /* {"Camera", "CameraTest"}, */
            {
                    //"heartrate", "HeartRateSensorTest"
                    "heartrate", "HeartRateSensorPah8001Test"
            },
            {
                    "stepcount", "StepCountTest"
            },
            {
                    "thpsensor", "THPSensorTest"
            },
            {
                    "temperaturesensor", "TemperatureSensorTest"
            },
            {
                    "humiditysensor", "HumiditySensorTest"
            },
            {
                    "pressuresensor", "PressureSensorTest"
            },
            {
                    "gps", "GPSTest"
            },
            {
                    "proximity", "ProximitySensorTest"
            },

            /*
             * Please put FactoryDataReset at the last item, it will clear
             * memory data, and restart the phone
             */
            {
                    "factory_data_reset", "FactoryDataReset"
            },
    };

    private class TestManuListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private List<Item> summary;
        private List<Integer> imgSource;

        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        public TestManuListAdapter(Context context, List<Item> summarys,
                List<Integer> imgSources) {
            mInflater = LayoutInflater.from(context);
            summary = summarys;
            imgSource = imgSources;
        }

        public int getCount() {
            return summary.size();
        }

        public Object getItem(int position) {
            return summary.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup par) {
            TestManuViewHolder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.test_list, null);
                holder = new TestManuViewHolder();
                holder.mImageView = (ImageView) convertView
                        .findViewById(R.id.check);
                holder.text2 = (TextView) convertView
                        .findViewById(R.id.TextView2);
                convertView.setTag(holder);
            } else {
                holder = (TestManuViewHolder) convertView.getTag();
            }

            holder.mImageView.setBackgroundResource(imgSource.get(position));
            holder.text2.setText(summary.get(position).translate);

            return convertView;
        }

        public class TestManuViewHolder {
            public TextView itemtitle;
            ImageView mImageView;
            TextView text2;
        }
    }

}
