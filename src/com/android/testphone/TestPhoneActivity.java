
package com.android.testphone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class TestPhoneActivity extends Activity {

    public static final String TEST_MANU = "com.android.testphone.TESTMANU";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Utils.isRoundScreen())
            setContentView(R.layout.test_phone_round);
        else
            setContentView(R.layout.test_phone_square);

        Button mBtManu = (Button) findViewById(R.id.btn_manu);
        Button mBtAuto = (Button) findViewById(R.id.btn_auto);
        mBtManu.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                startActivityWithMode(0);
            }
        });
        mBtAuto.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                startActivityWithMode(1);
            }
        });
    }

    private void startActivityWithMode(int value) {
        Intent intent = new Intent(TEST_MANU);
        intent.putExtra("mode", value);
        startActivity(intent);
    }
}
