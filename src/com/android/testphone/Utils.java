
package com.android.testphone;

import com.ingenic.iwds.BuildOptions;
import com.ingenic.iwds.HardwareList;

public class Utils {
    /**
     * 判断当前的屏是园还是方形
     * 
     * @return
     */
    public static boolean isRoundScreen() {
        return HardwareList.IsCircularScreen();
    }
}
