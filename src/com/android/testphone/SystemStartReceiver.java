
package com.android.testphone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

public class SystemStartReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            // When boot completed, reset the value of mmi_homekey_dispatched to
            // 0 for home key work.
            Log.d("SystemStartReceiver", "Test for MMI to reset MMI_HOMEKEY_DISPATCHED to 0.");
        }
    }

}
